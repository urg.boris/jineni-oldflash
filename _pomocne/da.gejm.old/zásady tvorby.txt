EDITOR

n�zvoslov�:
[objekt] 	- cokolif na hrac� plo�e na co je mo�n� kliknout, um�stit p�edm�t z invent��e (ko�, lampa....)	
id=obj31

[postava]	- lidi, v� jak, ne? Program je vid� stejn� jako [objekty]					
id=pos12

[p�edm�ty]	- v�cy v invent��i
id=item13

[textov� box]	- z�sobn�k textu s unik�tn�m id (nap� pos3_4A_n1_A), kter� p�i pr�chodu programu vyhod� obsa�en� text (nebo jeden z obsa�en�ch, pokud se text vyb�r� n�hodn� z v�ce mo�nosti viz. [n�hodn�])
<text id="pos3_4A_n1_A">

existuj� 4 typy rozhovorov�ch soubor�:
	posYX.xml - postavy
	objQW.xml - objekty na hrac� plo�e
	itemZG.xml - kecy ke kombinac�m
	universal.xml - obsahuje univerz�ln� odpov�di, nekombinace, nefunkce ..  . .  . 
		<text id="neninutno"> - nebudu to d�lat, nechci, to nejde... atd
		

- N�sledn�k - vytvo�� navazuj�c� textov� box

- Mo�nosti - rozv�tven� jednotliv�ch cest rozhovoru
		<text id="pos3_4">
			<moznost next="pos3_4_A" aktiv="0" id="pos3_4_A"></moznost>
			<moznost id="pos3_4_2" aktiv="1"></moznost>
		</text>
	!!!pozor: pokud je n�jak� z mo�nost� neaktivn� �i m� b�t n�sledn� deaktivov�na mus� m�t UNIK�TN� id(klidn� stejn� jako next)

- Newstart - zm�n� startovac� textov� box u dan� postavy

- Koment - vytvo�en� koment��e pro sna��� orientaci

- Zm�� n�co 
	- p�idat p�edm�t - do invent��e
	- odebrat p�edm�t - z invent��e
	- newstart - zm�n� startovac� textov� box u dan� postavy
	- otev�i lokaci - p��stupn�n� lokace na map�
	- spust funkci - specializovan� funkce, nap�. konec hry
	- zobraz objekt - objekt �i postava je viditeln� v dan� lokaci
	- skryjobjekt - objekt �i postava v dan� lokaci zmiz�
	- spust animaci - objekt �i postava zapne specializovanou animaci, �asto by m�lo b�t doprov�zeno newstartem(nap�. "nebudu ru�it")
	- next - p�ep�e ukazatel next dan�ho textov�ho boxu 
	- (de)aktivace mo�nosti - zobraz� �i skryje jednu v�b�rovou mo�nost dle unik�tn�ho id, nikolif dle id textov�ho boxu next

[ pokud nen� programu uvedeno jinak, zm�ny se v lokaci objev� okam�it� ]

- N�hodn� - do textov�ho boxu jsou vlo�eny n�hodn� mo�nosti. p�i pr�chodu textov�ho boxu program n�hodn� vybere jednu z mo�nost�

- odkaz - na m�sto kde v editoru le�� kurzoru( nebo p�es vybranou oblast v textu), vlo�� ze seznamu lokac� �i postav, p�edm�t� p��slu�n� id

- Samomluva - u nov� vlo�en�ch textov�ch box� se nest��d�j� postavy, postava mluv� sama (hlavn� pro popis ter�nu, �i p�edm�t� na hrac� plo�e)

- zp�t - jeden krok zp�t

[ KOMBINACE P�EDM�T� S OSTATN�MI OBJEKTY, POSTAVAMI ]
-kombinace p�edm�t+p�edm�t - samostatn� soubor "item12.xml"; kombinace p�edm�t+objekt/postava soubor "obj12.xml"/"pos43.xml"

-id textov�ho boxu je ve form�tu "item1_pos3_1", "item13_obj42_1"

-pro kombinaci p�edm�t, p�edm�t: "item1_item2_1", rostouc� posloupnost n�zv� p�edm�t� invent��e je nutn� (nen� mo�no napsat "item21_item5_1")

-textov� boxy pro odm�tav� odpov�di postav �i objekt� na jak�koliv p�edm�t z invent��e maj� toto id "pos13_nechci_1", "obj33_nechci_1", pochopiteln� je mo�no um�stit do boxu mnoho n�hodn�ch reakc�(tla��tko N�hodn�)

p��klady: 	univerz�ln� kecy pro kombinaci postava14+libovoln� p�edm�t - id="pos14_nechci_1"
		univerz�ln� kecy pro kombinov�n� jak�hokoliv p�edm�tu s p�edm�tem 26 - id="item26_nechci_1"

-ve v�sledku se soubory slou��, tak�e nen� nutno dodr�ovat rozmis�ov�n� do p��slu�n�ch soubor�, p�ijde mi to v�ak orienta�n� p�ijateln�j��


ANIMACE

pro dropTarget.parent mus� b�t instance postav pojmenov�ny i v druh� �rovni (pos1.pos1)

