package Game.GameObject 
{
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	public class Location 
	{
		private var _id:String;
		private var _nameId:String;
		private var _names:Array = new Array;
		private var _isOpened:Boolean = false;
		private var _isOnMap:Boolean = false;
		private var _objects:Array;
		private var _exits:Array;

		
		public static const ID:String = "id";
		public static const NAME:String = "name";
		public static const ACTIVE:String = "active";
		public static const IS_OPENED:String = "isOpened";
		public static const IS_ON_MAP:String = "isOnMap";
		public static const OBJECT:String = "object";
		public static const LOCATION:String = "exit";
		
		public function Location(locationXML:XML) 
		{
						trace(locationXML.toXMLString());
			trace("-------------------");
			if (!hasAttributeWithName(locationXML, ID)) {
				throw new WronglyFormattedDialogXML("attribute @" + ID + " nanalezen v lokaci " + locationXML.toXMLString());
			}			
			this._id = locationXML.@[ID];			
			
	/*		if (!hasChildWithName(locationXML, "names")) {
				throw new WronglyFormattedDialogXML("child " + NAME + " nanalezen v lokaci id=" + this._id);
			}*/	
			//trace(locationXML.name.toXMLString());
			
			for each (var name:XML in locationXML.names.elements('name'))
			{
				//trace("asdasdasdas");//int(name.@[ID]), name.text()
				//this._locationNames[int(location.@[Replic.ID])] = location.name.text();
				this._names[int(name.@[ID])] = name.text();
				//TODO new location from xml
				
			}

			trace(this._names);
			//this._nameId = locationXML.@[NAME];			
			
/*			if (!hasOnlyChildWithName(replicXML, NEXT)) {
				throw new WronglyFormattedDialogXML("jine mnozstvi " + NEXT + " potomku nez 1 v replice id=" + this._id);
			}
			this._next = replicXML.elements(NEXT);
			
			if (!hasOnlyChildWithName(replicXML, TEXT)) {
				throw new WronglyFormattedDialogXML("jine mnozstvi " + TEXT + " potomku nez 1 v replice id=" + this._id);
			}
			this._text = replicXML.elements(TEXT);
			
			var triggersCount:int = countChildrenWithName(replicXML, TRIGGER);
			if (triggersCount > 1) {
				throw new WronglyFormattedDialogXML("vetsi mnozstvi " + TRIGGER + " potomku nez 1 v replice id=" + this._id);
			}
			if (triggersCount == 1) {
				this._triggers = replicXML.elements(TRIGGER);
			}
			
			if (hasAttributeWithName(replicXML, IS_ENTRY_POINT)) {
				if(Controller.DEBUG) {
					trace("this is EP " + replicXML.@id);
				}
				this._isEntryPoint = true;
			}*/
		}
		
		private function hasChildWithName(locationXML: XML, name:String):Boolean {
			return locationXML.child(name).length() > 0;//(replicXML.(hasOwnProperty('@' + name)) && replicXML.@[name].toXMLString() != "");
		}
		
		private function hasAttributeWithName(replicXML: XML, name:String):Boolean {
			return replicXML.attribute(name).length() > 0;//(replicXML.(hasOwnProperty('@' + name)) && replicXML.@[name].toXMLString() != "");
		}
		
	}

}