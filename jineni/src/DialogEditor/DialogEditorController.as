package DialogEditor
{	
	import Common.Controller.ControllerWithFilesLoader;
	import Common.Controller.IController;
	import Common.World.CoreGameObjects;
	import Common.World.GameObject.Item;
	import DialogEditor.DialogObject.Dialog;
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	import Common.Events.EventManager;
	import Common.Exception.RuntimeInternalError;
	import Common.Utils.Checker;
	import Common.World.AllGameObjectsOLD;
	import Common.Utils.XmlToolsForXMLList;
	import DialogEditor.Utils.StringConst;
	import DialogEditor.Exception.IncorectInput;
	import DialogEditor.Utils.Formatter;
	import DialogEditor.Wizards.AddMoreReplicsWizzard;
	import DialogEditor.Wizards.AddReplicWizzard;
	import DialogEditor.Wizards.AddTriggerWizzard;
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;
	import fl.containers.ScrollPane;
	import fl.controls.Button;
	import fl.controls.CheckBox;
	import fl.controls.List;
	import fl.controls.ScrollPolicy;
	import fl.controls.UIScrollBar;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.text.TextField;
	import DialogEditor.Utils.EditorTools;
	import Common.Utils.GeneralTools;
	import flash.net.registerClassAlias;
	//!! musi byt soucasti swc souboru, swc soubor musi existovat - obsahovat symboly 
	
	public class DialogEditorController extends ControllerWithFilesLoader implements IController
	{
		//TODO confirm event - treba na back
		//TODO history event 
		//todo pryc
		//private var _loadedSWF:Sprite;
		private var _mainEventHandlerData: Array;
		
		private var _dialog:Dialog;
		private var _dialogDrawer:DialogEditor.DialogDrawer;
		private var _dialogOverview:ScrollPane;
		private var _editorTextField:TextField;

		private var _editorTextBack:String;
		private var _editorTextBack2:String;
		private var _newDialog:XML;
		
		private var _infoMessageField:TextField;
		private var _editorTextScroller:UIScrollBar;
		private var _fileRef:FileReference;
		public var _fileName:String;
		public var _optionsBox:List;

		private var _addReplicsWizard:Sprite;
		private var _addTriggerWizzard: AddTriggerWizzard;
		private var _addReplicWizard: AddReplicWizzard;
		private var _addMoreReplicsWizard: AddMoreReplicsWizzard;
		
		
		
		public function DialogEditorController(main:Main)
		{
			var loader:BulkLoader = new BulkLoader("xmls");
			super(main, loader);
		}
		
		public function run():void
		{
			_mainEventHandlerData = new Array(
				{spriteName: "addEntryPoint", eventHandlerName: "onAddEntryPoint", type: Button}, 
				{spriteName: "addReplic", eventHandlerName: "onAddReplic", type: Button}, 
				{spriteName: "addReplics", eventHandlerName: "onAddMoreReplics", type: Button}, 
				{spriteName: "addComment", eventHandlerName: "onAddComment", type: Button}, 
				{spriteName: "addTrigger", eventHandlerName: "onAddTrigger", type: Button},
				{spriteName: "integrityCheck", eventHandlerName: "onIntegrityCheck", type: Button}, 
				{spriteName: "drawDialog", eventHandlerName: "onDrawDialog", type: Button}, 
				{spriteName: "loadFile", eventHandlerName: "onLoadXmlDialogFile", type: Button}, 
				{spriteName: "saveFile", eventHandlerName: "onSaveXmlDialogFile", type: Button}, 
				{spriteName: "locationsLinks", eventHandlerName: "onChangeLinkList", type: Button}, 
				{spriteName: "charactersLinks", eventHandlerName: "onChangeLinkList", type: Button}, 
				{spriteName: "itemsLinks", eventHandlerName: "onChangeLinkList", type: Button},
				{spriteName: "backButton", eventHandlerName: "onBackChange", type: Button},
				{spriteName: "highlightEditorText", eventHandlerName: "onHightlightEditorText", type: Button},
				{spriteName: "newFile", eventHandlerName: "onNewFile", type: Button}
			);
			
			ConfigXMLSettings();			
			//AddCoreXmlFiles();			
			_loader.add("xml/dialog/dialog.xml", {id: "dialog", type: BulkLoader.TYPE_XML});	
			_loader.add("xml/editor/newDialog.xml", {id: "newDialog", type: BulkLoader.TYPE_XML});
			_loader.add("graphic/DialogEditor.swf", {id: "graphic", type: BulkLoader.TYPE_MOVIECLIP});			
			RunLoader();
		}
		
		override protected function onAllLoaded(evt:BulkProgressEvent):void
		{
			CreateCoreGameObjects();

			var loadedSWF:Sprite = evt.target.getMovieClip("graphic") as Sprite;
			SetSprite(loadedSWF);
			_infoMessageField = GetObjectInLoadedGraphic("infoMessage", TextField);
			
			_newDialog = evt.target.getXML("newDialog") as XML;
			
			ShowInfoMessage("all files loaded");
			
			var dialogXML:XML = evt.target.getXML("dialog") as XML;
			
			var sprite:Sprite = GetSpriteInLoadedGraphic("addReplicWizard");
			_addReplicWizard = new AddReplicWizzard(this, sprite);
			
			sprite = GetSpriteInLoadedGraphic("addMoreReplicsWizard");
			_addMoreReplicsWizard = new AddMoreReplicsWizzard(this, sprite);
			 
			sprite = GetSpriteInLoadedGraphic("addTriggerWizzard");
			_addTriggerWizzard = new AddTriggerWizzard(this, sprite);
			
			//set replic graph overview
			_dialogOverview = GetObjectInLoadedGraphic("DialogOverview", ScrollPane);
			_dialogOverview.verticalScrollPolicy = ScrollPolicy.ON;
			_dialogOverview.horizontalScrollPolicy = ScrollPolicy.ON;
		
			//var dialogXMLString:XML = new XML(GeneralTools.DecodeToString(dialogXML));

			_dialog = new Dialog(dialogXML, _coreGameObjects);
			_dialogDrawer = new DialogEditor.DialogDrawer(_dialog);
			tryDrawDialog(false);
			
			//set xml texty
			//!!! font musi byt embeded v swc souboru
			_editorTextScroller = GetObjectInLoadedGraphic("editorTextScroller", UIScrollBar);
			_editorTextField = GetObjectInLoadedGraphic("editorText", TextField);

			ReflectDialogXmlAsEditorText();
			HighlightAll();
			
			//init for back button
			_editorTextBack = _editorTextField.text;
			

			//TODO experimental
			_editorTextField.addEventListener(Event.CHANGE, onEditorTextChange);
			
			
			_optionsBox = GetObjectInLoadedGraphic("optionsBox", List);
			_optionsBox.dataProvider = _coreGameObjects.GetItemsData();
			_optionsBox.addEventListener(Event.CHANGE, onOptionBoxClick);			
			
			SetEventManagement(loadedSWF, _mainEventHandlerData, false);
			
			var blo:Item = _coreGameObjects.GetItem("I22");
			trace(blo.GetWholeName());
			
			//TODO zamyslet se jestli nemit serializovany a naloadovat
			/* !! pro serializaci
			//!! nutnost registerClassAlias jinak chyba
			registerClassAlias("AllGameObjects", AllGameObjects);			
			_allGameObjects = new AllGameObjects();
			GeneralTools.SerializeObjectToFile(_allGameObjects,"world/allGameObjects.ble")			
			_allGameObjects = GeneralTools.DeserializeObjectFromFile("world/allGameObjects.ble") as AllGameObjects;
			_allGameObjects.TraceContent();
			*/
			
			this._main.addChild(loadedSWF);
		}
		
		private function onEditorTextChange(event:Event):void {
			makeHistoryStep();
		}
		
		private function makeHistoryStep():void {
			_editorTextBack2 = _editorTextBack;
			_editorTextBack = _editorTextField.text;
		}
		
		public function onBackChange(event:Event):void {
			_editorTextField.text = _editorTextBack2;
			if (CheckXMLValidy(true)) {
				_dialog.setDialogXmlFromString(_editorTextField.text);
				HighlightAll();
			}
		}
		
		public function onHightlightEditorText(event:Event):void {
			if (CheckXMLValidy(true)) {
				_dialog.setDialogXmlFromString(_editorTextField.text);
				HighlightAll();
			}
		}
		
		public function onNewFile(event:Event):void {
			_dialog.setDialogXml(_newDialog);
			tryDrawDialog();
			ReflectDialogXmlAsEditorText();
			makeHistoryStep();
			HighlightAll();
		}
		
		public function onLoadXmlDialogFile(evt:MouseEvent):void  // =null
		{
			_fileRef = new FileReference();
			_fileRef.addEventListener(Event.SELECT, onLoadXmlDialogFileSelected);
			_fileRef.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			var textTypeFilter:FileFilter = new FileFilter("Dialogové soubory", "*.xml");
			
			_fileRef.browse([textTypeFilter]);
		}
		
		protected function onIOError(evt:IOErrorEvent):void
		{
			trace("There was an IO Error.");
		}
		
		private function onLoadXmlDialogFileSelected(evt:Event):void
		{
			_fileRef.addEventListener(Event.COMPLETE, onLoadXmlDialogFileComplete);
			_fileRef.load();
		}
		
		private function onLoadXmlDialogFileComplete(evt:Event):void
		{
			_dialog.tryToSetDialogXml(new XML(_fileRef.data));
			ShowInfoMessage("file " + _fileRef.name + " loaded");
			_fileName = _fileRef.name;
			ReflectDialogXmlAsEditorText();
			HighlightAll();
			tryDrawDialog();
			_fileRef.removeEventListener(Event.SELECT, onLoadXmlDialogFileSelected);
			_fileRef.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			_fileRef.removeEventListener(Event.COMPLETE, onLoadXmlDialogFileComplete);
		}
		
		public function onSaveXmlDialogFile(evnt:Event = null):void
		{
			var fileRef:FileReference = new FileReference();
			var fileName:String = "xxx.xml";
			if (_fileName != null)
			{
				fileName = _fileName;
			}
			fileRef.addEventListener(Event.COMPLETE, onSaveXmlDialogFileComplete);
			fileRef.save(String(_editorTextField.text), fileName);
		}
		
		private function onSaveXmlDialogFileComplete(evt:Event):void
		{
			_fileName = evt.target.name;
			ShowInfoMessage("file " + evt.target.name + " saved");
		}

		
		public function onOptionBoxClick(event:Event):void
		{
			var length:int = _editorTextField.text.length;
			
			//!! length>0 = nahrad, pokud length==0 muze jen caretpos, nebo _editorTextField.selectionBeginIndex == length && _editorTextField.selectionEndIndex == length
			if (_editorTextField.selectedText.length > 0){
				_editorTextField.replaceSelectedText(event.target.selectedItem.data);
				//!!prevent cykles of unwantingness
				_editorTextField.setSelection(length, length);
			} else if (_editorTextField.selectedText.length == 0) {
				if (_editorTextField.selectionBeginIndex < length) {
					_editorTextField.text = _editorTextField.text.slice(0, _editorTextField.caretIndex) + event.target.selectedItem.data + _editorTextField.text.slice(_editorTextField.caretIndex);
					HighlightAll();
					//!!prevent cykles of unwantingness
					_editorTextField.setSelection(length, length);
				} else {
					//tohle else by melo fungovat jako if (!(_editorTextField.selectionBeginIndex == length && _editorTextField.selectionEndIndex == length)) {
					//!!pri init loadu je select nastavenej na delku textu, proti padani
					ShowInfoMessage("text k nahrazeni nevybran");
				}
			} else {
				ShowInfoMessage("text k nahrazeni nevybran");
			}
		}
		/*
		private function transformXmlToList(xmlData:XML):DataProvider
		{
			var itemLabel:String = "";
			var itemData:String;
			var list:DataProvider = new DataProvider();
			var subLength:int;
			for (var i:int = 0; i < xmlData.children().length(); i++)
			{
				subLength = xmlData.children()[i].elements(XmlTypeNames.NAME).length();
				for (var j:int = 0; j < subLength; j++)
				{
					itemLabel += xmlData.children()[i].elements(XmlTypeNames.NAME)[j];
					if (j != subLength - 1)
					{
						itemLabel += ", ";
					}
				}
				itemData = new String(xmlData.children()[i].@id);
				itemLabel = itemData+" - "+itemLabel;
				list.addItem({label: itemLabel, data: itemData});
				itemLabel = "";
			}
			return list;
		}
		*/
		public function ReflectDialogXmlAsEditorText():void
		{
			_editorTextField.text = _dialog.getContent().toXMLString();
			kickEditorTextScroller();
		}
		
		private function ReflectEditorTextAsDialogXmlIfPossible():void
		{
			_dialog.setDialogXmlFromString(_editorTextField.text);
		}
		
		public function ShowInfoMessage(message:String):void
		{
			_infoMessageField.text = message;
		}
		
		public function onDrawDialog(evnt:MouseEvent):void
		{
			try
			{
				ReflectEditorTextAsDialogXmlIfPossible();
				DrawDialog();
			}
			catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}
		
		private function DrawDialog(reflectEditorText:Boolean=true):void
		{
			if(reflectEditorText==true) {
				//!! aby bralo v potaz predchozi zmeny v editor textu
				ReflectEditorTextAsDialogXmlIfPossible();
			}
			
			//!! for unwanted glitches reconstruct dialog and drawer
			_dialog = new Dialog(_dialog.getContent(), _coreGameObjects);
			_dialogDrawer = new DialogEditor.DialogDrawer(_dialog);
			_dialogDrawer.drawDialog();
			_dialogOverview.source = _dialogDrawer;
			/*
			_dialogOverview.height = _dialogOverview.height +50; 
			_dialogOverview.width = _dialogOverview.width +50; 
			*/
		}
		
		private function tryDrawDialog(reflectEditorText:Boolean=true):Boolean
		{
			try
			{
				DrawDialog(reflectEditorText);
				return true;
			}
			catch (e:Error)
			{
				ShowInfoMessage("Nelze vykleslit graf dialogu: " + e.message);
				return false;
			}
			return true;
		}
		
		public function onIntegrityCheck(evnt:MouseEvent):void
		{
			try
			{
				//!! aby bralo v potaz predchozi zmeny v editor textu
				ReflectEditorTextAsDialogXmlIfPossible();
				
				//!!valid XML
				var editorText:XML = XML(_editorTextField.text);
				//!!could be drawn=ids check
				ReflectEditorTextAsDialogXmlIfPossible();
				DrawDialog();
				//!!check redundant
				var redundantReplics:Array = _dialogDrawer.getRedundantReplicNames();				
				if (redundantReplics.length > 0) {
					throw new RuntimeInternalError("redundantni repliky:"+redundantReplics.join(", "));
				}
				//TODO zkontrolovat jetli maji vsechny repliky, spravny pocet potomku-napr. 1 text 1-n replicNode v pointsTo
				//TODO unknown attribute
				ShowInfoMessage("alles fertig");
			}
			catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}		

		
		private function HighlightAll():void
		{
			var editorText:String = _dialog.getContent().toXMLString();
			_editorTextField.htmlText = Formatter.HightlightAll(editorText);
		}
		
		public function GetReplicAsXMLListById(id:String):XMLList {
			return _dialog.getReplicAsXMLListById(id);
		}
		
		public function GetReplicAsXMLById(id:String):XML {
			return _dialog.getReplicAsXMLById(id);
		}
		
		private function GetActualReplicId(): String {
			var startIndex:int = _editorTextField.text.lastIndexOf(StringConst.SEARCH_REPLIC_ID, _editorTextField.caretIndex);
			startIndex += StringConst.SEARCH_REPLIC_ID.length + 1;
			var endIndex:int = _editorTextField.text.indexOf("\"", (startIndex + 1));
			var replicId:String = _editorTextField.text.substring(startIndex, endIndex);			
			if (replicId.indexOf(StringConst.REPLIC_ID_SEPARATOR) == -1)
			{
				throw new WronglyFormattedDialogXML("getActualReplicId: char \""+StringConst.REPLIC_ID_SEPARATOR+"\" not present in " + replicId);
			}
			return replicId;
		}
		
		private function GetActualAndNextReplicIds(): Array {			
			var replicId:String = GetActualReplicId();			
			var splittedReplicId:Array = replicId.split(StringConst.REPLIC_ID_SEPARATOR);
			var nextReplicLastNum:int = int(splittedReplicId[(splittedReplicId.length - 1)]) + 1;
			splittedReplicId.pop();
			splittedReplicId.push(nextReplicLastNum);
			var nextReplicId:String = splittedReplicId.join(StringConst.REPLIC_ID_SEPARATOR);
			
			return new Array(replicId, nextReplicId); 
		}
		
		private function replaceInEditorText(startIndex:int, find:String, replaceWith:String):void
		{
			var searchPositionStartIndex:int = _editorTextField.text.indexOf(find, startIndex);
			var searchPositionEndIndex:int = searchPositionStartIndex + find.length;
			_editorTextField.setSelection(searchPositionStartIndex, searchPositionEndIndex);
			_editorTextField.replaceSelectedText(replaceWith);
			_editorTextField.setSelection(searchPositionStartIndex, searchPositionStartIndex);
		}
		
		private function kickEditorTextScroller():void
		{
			_editorTextScroller.scrollTarget = _editorTextField;
			_editorTextScroller.update();
		}
		
		private function SrollEditorTextABit():void
		{
			kickEditorTextScroller();
			_editorTextScroller.scrollPosition += 3;
		}
		
		public function CheckXMLValidy(noticeIfValid:Boolean = false):Boolean
		{
			try
			{
				var editorText:XML = XML(_editorTextField.text);
				if (noticeIfValid)
				{
					ShowInfoMessage("validni xml");
				}
				return true;
			}
			catch (e:Error)
			{
				ShowInfoMessage(e.message);
				return false;
			}
			return false;
		}
		
		public function onChangeLinkList(event:Event):void
		{
			switch (event.target.name)
			{
				case "locationsLinks": 
					_optionsBox.dataProvider = _coreGameObjects.GetLocationsData();
					break;
				case "charactersLinks": 
					_optionsBox.dataProvider = _coreGameObjects.GetDialogObjectsData();
					break;
				case "itemsLinks": 
					_optionsBox.dataProvider = _coreGameObjects.GetItemsData();
					break;
				case "dilogFlowLinks": 
					//_optionsBox.dataProvider = _possibleDialogFlowChanges;
					break;
				default:
					throw new RuntimeInternalError("unknown switch option error");
					break;
			}
		}
		
		/***********************************************************************************
		 * ENRTY POINT + ADD REPLIC
		 ***********************************************************************************/
		
		public function onAddEntryPoint(evnt:MouseEvent):void
		{
			try {
				//!! aby bralo v potaz predchozi zmeny v editor textu
				ReflectEditorTextAsDialogXmlIfPossible();
				
				_eventManager.RemoveListeners();
				var actualAndNextReplicId:Array = GetActualAndNextReplicIds();
				_addReplicWizard.Open(actualAndNextReplicId, 14, "blabla", true);
				//_addReplicWizard.AttachListeners();
			} catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}		
		
		public function onCloseWizard():void {
			_eventManager.AttachListeners();
		}
		
		public function onAttachReplicWizzard(originReplicId:String, newReplic:XML): void {
			try {
/*
				//!! aby bralo v potaz predchozi zmeny v editor textu
				reflectEditorTextAsDialogXmlIfPossible();
				*/
				
				ReflectDialogXmlAsEditorText();
				
				_dialog.attachReplicAfterId(originReplicId, newReplic);
				SrollEditorTextABit();
				ReflectDialogXmlAsEditorText();
				HighlightAll();

				_addReplicWizard.Close();

				//_main._stage.focus = _editorTextField;
				
			} catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}

		public function onAddReplic(evnt:MouseEvent):void
		{
			try
			{
				//TODO check jestli takle spravne?
				_eventManager.RemoveListeners();
				var actualAndNextReplicId:Array = GetActualAndNextReplicIds();
				_addReplicWizard.Open(actualAndNextReplicId, 14, "blabla", false);
			}
			catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}
		
		/***********************************************************************************
		 * ADD MORE REPLICS
		 ***********************************************************************************/

		public function onAddMoreReplics(evnt:MouseEvent):void
		{
			try
			{
				_eventManager.RemoveListeners();
				var actualAndNextReplicId:Array = GetActualAndNextReplicIds();
				_addMoreReplicsWizard.Open(actualAndNextReplicId, "blabla", false);
			}
			catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}
		
		public function onAttachAddMoreReplicsWizard(replicIdAndNextId:Array, newReplic:XML, howMuch:int):void {					
			try {
				//!! aby bralo v potaz predchozi zmeny v editor textu
				//reflectEditorTextAsDialogXmlIfPossible();
				
				//??TODO nechat tady? nebo az resit az na konci, asi staci dole
				//_addMoreReplicsWizard.RemoveListeners();
				ReflectDialogXmlAsEditorText();

				var replicId:String = replicIdAndNextId[0];
				var nextReplicId:String = replicIdAndNextId[1];

				_dialog.attachReplicAfterId(replicId, newReplic);
				
				var prevReplicOptionId:String = nextReplicId;
				var newReplicOptionId:String;
				for (var i:int = 1; i <= howMuch; i++)
				{
					//!!aby vznikla hluboka kopie
					newReplic = new XML();
					newReplicOptionId = nextReplicId + String.fromCharCode(i + 64) + StringConst.REPLIC_ID_SEPARATOR + "1";
					newReplic.@id = newReplicOptionId;
					newReplic = EditorTools.createNewReplicXML(newReplicOptionId, "P0", StringConst.UNFILLED);
					_dialog.attachReplicAfterId(prevReplicOptionId, newReplic);
					prevReplicOptionId = newReplicOptionId;
					SrollEditorTextABit();
				}
				
				ReflectDialogXmlAsEditorText();
				HighlightAll();
				tryDrawDialog();
				
				_addMoreReplicsWizard.Close();					
				_main._stage.focus = _editorTextField;
				
			} catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}
		
		/***********************************************************************************
		 * COMMENT
		 ***********************************************************************************/
		
		public function onAddComment(evnt:MouseEvent):void
		{
			addComment();
		}
		
		public function addComment():void
		{
			if (!CheckXMLValidy())
			{
				return;
			}
			
			var replicId:String = GetActualReplicId();
			
			//!!! nasledujici musi jit presne v tomto poradi jinak se rozsipe
			var newNode:XML = XML("<!-- " + StringConst.SEARCH_TAG + " -->");
			_dialog.attachCommentBeforeId(replicId, newNode);
			ReflectDialogXmlAsEditorText();
			HighlightAll();
			replaceInEditorText(0, StringConst.SEARCH_TAG, "");
			_main._stage.focus = _editorTextField;
			//!!! zasebou jdouci
		}
		
		/***********************************************************************************
		 * TRIGGER
		 ***********************************************************************************/

		public function onAddTrigger(event:Event):void
		{
			try
			{
				_eventManager.RemoveListeners();
				var replicId:String = GetActualReplicId();
				//trace(replicId);
				_addTriggerWizzard.Open(replicId);
			}
			catch (e:Error)
			{
				ShowInfoMessage(e.message);
			}
		}
		
		public function GetReplicsWithMultiplePointsTo():Array {
			return _dialog.GetReplicsWithMultiplePointsTo();
		}
		
		public function GetPointsToIdsFrom(id:String):Array {
			return _dialog.GetPointsToIdsFrom(id);
		}
		
		
		
		public function onAttachTrigger(attachToReplicId:String, trigger:XML):void
		{
			var actualReplic:XMLList = _dialog.getReplicAsXMLListById(attachToReplicId);
			var triggerBlock:XML;
			
			if (!XmlToolsForXMLList.hasElement(actualReplic, StringConst.TRIGGER)) {
				triggerBlock = actualReplic.appendChild(new XML("<" + StringConst.TRIGGER + "/>"));
			}
			triggerBlock = actualReplic.elements(StringConst.TRIGGER)[0];
			triggerBlock.appendChild(trigger);
			
			
			ReflectDialogXmlAsEditorText();
			SrollEditorTextABit();
			HighlightAll();
			_addTriggerWizzard.Close();
			//_main._stage.focus = _editorTextField;
		}	
	}

}