package DialogEditor.Exception 
{
	public class DialogEditorError extends Error 
	{ 
		public function DialogEditorError(message:String) 
		{ 
			super(message); 
		} 
	}

}