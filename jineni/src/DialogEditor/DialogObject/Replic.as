package DialogEditor.DialogObject
{
	import DialogEditor.DialogEditorController;
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	import flash.display.Sprite;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	//import Common.Controller.BaseController;
	import DialogEditor.Utils.StringConst;
	import Common.Utils.XmlToolsForXMLList;
	
	public class Replic extends Sprite
	{
		private var _id:String;
		private var _text:String;
		private var _triggers:XMLList;
		private var _objectId:String;
		private var _next:XMLList;
		private var _isEntryPoint: Boolean = false;
		private var _successorsCount:int = 0;
		private var _successors:Array = new Array;
		private var _isDialogEnd:Boolean = true;
		private var _dialog: Dialog;
		private var _isRandomPointer: Boolean = false;
		//private var _successorsIds:Array = new Array;
		
		
		public function Replic(replicXML:XMLList, dialog: Dialog)//Replic(replicXML:XMLList, dialog: Dialog)
		{
			super();
			
			_dialog = dialog;
			
			if (!XmlToolsForXMLList.hasAttributeWithName(replicXML, StringConst.ID)) {
				throw new WronglyFormattedDialogXML("attribute @" + StringConst.ID + " nanalezen v replice " + replicXML.toXMLString());
			}			
			_id = replicXML.@[StringConst.ID];			
			
			//!!! z XML rozhovoru leze sice character id, ale dale je programem chapano jako objectId
			if (!XmlToolsForXMLList.hasAttributeWithName(replicXML, StringConst.DIALOG_OBJECT)) {
				throw new WronglyFormattedDialogXML("attribute @" + StringConst.DIALOG_OBJECT + " nanalezen v replice id=" + _id);
			}			
			_objectId = replicXML.@[StringConst.DIALOG_OBJECT];
			
			if (!XmlToolsForXMLList.hasOnlyChildWithName(replicXML, StringConst.POINTS_TO)) {
				throw new WronglyFormattedDialogXML("jine mnozstvi " + StringConst.POINTS_TO + " potomku nez 1 v replice id=" + _id);
			}
			_next = replicXML.elements(StringConst.POINTS_TO);
			
			if (!XmlToolsForXMLList.hasOnlyChildWithName(replicXML, StringConst.TEXT)) {
				throw new WronglyFormattedDialogXML("jine mnozstvi " + StringConst.TEXT + " potomku nez 1 v replice id=" + _id);
			}
			this._text = replicXML.elements(StringConst.TEXT);
			
			var triggersCount:int = XmlToolsForXMLList.countChildrenWithName(replicXML, StringConst.TRIGGER);
			if (triggersCount > 1) {
				throw new WronglyFormattedDialogXML("vetsi mnozstvi " + StringConst.TRIGGER + " potomku nez 1 v replice id=" + _id);
			}
			if (triggersCount == 1) {
				_triggers = replicXML.elements(StringConst.TRIGGER);
			}
			
			if (XmlToolsForXMLList.attributeEqualsTo(replicXML, StringConst.IS_ENTRY_POINT, "1")) {
				this._isEntryPoint = true;
			}
			
			if (this._next.children()[0].name() == StringConst.DIALOG_END) {
				/*if(BaseController.DEBUG) {
					trace("this is EndDialog " + replicXML.@id);
				}*/
				_isDialogEnd = true;
			} else {
				_isDialogEnd = false;
			}
			
			if (XmlToolsForXMLList.attributeEqualsTo(replicXML, StringConst.IS_RANDOM_POINTER, "1")) {
				this._isRandomPointer = true;
			}
			createVisual();
		}
		
		public function dialogContinues():Boolean {
			return !this._isDialogEnd;
		}
		
		public function isDialogEnd():Boolean {
			return this._isDialogEnd;
		}
		
		public function isRandomPointer():Boolean {
			return _isRandomPointer;
		}
		
		public function createVisual():void
		{
			var textField:TextField = new TextField();
			textField.autoSize = TextFieldAutoSize.LEFT;
			textField.background = true;
			textField.border = true;
			textField.text = _id + " (" + _dialog.GetDialogObjectNameById(_objectId) + "):\n" + _text;			
			
			var format:TextFormat = new TextFormat();
			format.font = "Verdana";
			format.color = 0xFF0000;
			format.size = 10;
			format.underline = true;
			
			textField.defaultTextFormat = format;
			this.addChild(textField);
		}
		
		public function getId():String 
		{
			return this._id;
		}
		
		public function getNext():XMLList 
		{
			return this._next;
		}
		
		public function getSuccessorsCount():int
		{
			return this._successorsCount;
		}		
		
		public function addSuccessor(replic:Replic):void {
			this._successors.push(replic);
			this._successorsCount += replic.getSuccessorsCount() + 1;
		}
		
		public function getSuccessors(): Array {
			return this._successors;
		}
		
		public function isEntryPoint():Boolean {
			return this._isEntryPoint;
		}
	}
}