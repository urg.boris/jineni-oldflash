package DialogEditor.DialogObject
{
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	import Common.Exception.RuntimeInternalError;
	import Common.World.AllGameObjectsOLD;
	import DialogEditor.Utils.StringConst;
	import Common.World.CoreGameObjects;
	
	public class Dialog
	{
		private var _replics:Array = new Array;
		private var _entryPoints:Array = new Array;
		
		private var _dialogXML:XML;
		private var _coreObjectsNames:CoreGameObjects;
		
		public function Dialog(dialogXML:XML, gameObjects:CoreGameObjects)
		{
			_coreObjectsNames = gameObjects;
			if (dialogXML.toXMLString() === "")
			{
				throw new RuntimeInternalError("loaded dialog is empty");
			}
			_dialogXML = dialogXML;

			//createObjectNamesFromXML();
			//createLocationNamesFromXML();
			
		}
		public function findEntryPointsInXML():XMLList
		{			
			return this._dialogXML.replic.(hasOwnProperty('@' + StringConst.IS_ENTRY_POINT) && @[StringConst.IS_ENTRY_POINT] == '1');
		}
		
		public function addReplic(replica:Replic):void
		{

			this._replics.push(replica);
		}
		
		public function addEntryPoint(replica:Replic):void
		{
			this._entryPoints.push(replica);
		}
		
		public function getEntryPoints():Array
		{
			return this._entryPoints;
		}
		
		public function getAllReplics(): Array {
			var allReplics:XMLList = this._dialogXML.replic;
			var replicIds:Array = new Array();
			for each(var replic:XML in allReplics) {
				replicIds.push(replic.@id.toString());
			}
			return replicIds;
		}
		
		public function replicExists(id:String): Boolean {
			var replicXML:XMLList = this._dialogXML.replic.(@[StringConst.ID] == id);
			if (replicXML.toXMLString()!="") {
				return true;
			} else {
				return false;
			}
		}
		
		public function getReplicAsXMLListById(id:String):XMLList {
			var replicXML:XMLList = this._dialogXML.replic.(@[StringConst.ID] == id);
			if (this._dialogXML.replic.(@[StringConst.ID] == id).length() > 1) {
				throw new WronglyFormattedDialogXML("replic id=" + id + " se vyskytuje vicekrat");
			}
			if (replicXML.toXMLString() == "")
			{
				throw new WronglyFormattedDialogXML("replic id=" + id + " neexistuje");
			}
			else
			{	
				return replicXML;
			}
		}
		
		public function getReplicAsXMLById(id:String):XML {
			var replicsXML:XMLList = this._dialogXML.replic.(@[StringConst.ID] == id);
			if (replicsXML.length() > 1) {
				throw new WronglyFormattedDialogXML("replic id=" + id + " se vyskytuje vicekrat");
			}
			if (replicsXML.toXMLString() == "")
			{
				throw new WronglyFormattedDialogXML("replic id=" + id + " neexistuje");
			}
			else
			{	
				return replicsXML[0];
			}
		}
		
		public function GetReplicTextById(id:String):String {
			return getReplicAsXMLById(id).elements("text").toString();
		}
		
		public function GetDialogObjectNameById(id:String):String
		{
			return _coreObjectsNames.GetDialogObject(id).GetWholeName();
		}
		
		public function createReplicFromXMLById(id:String):Replic {
			return new Replic(getReplicAsXMLListById(id), this);
		}
		
		public function deleteReplicFromXmlById(id:String):void
		{
			var replicXML:XMLList = this._dialogXML.replic.(@[StringConst.ID] == id);
			if (replicXML.toXMLString() == "")
			{
				throw new WronglyFormattedDialogXML("replic id=" + id + " neexistuje");
			}
			delete this._dialogXML.replic.(@[StringConst.ID] == id)[0];
		}
		
		public function attachReplicAfterId(id:String, replic:XML):void {
			//trace(_dialogXML.replic.(@id == id).toXMLString());
			_dialogXML.insertChildAfter(_dialogXML.replic.(@id == id)[0], replic);
		}
		
		public function attachCommentBeforeId(id:String, comment:XML):void {
			_dialogXML.insertChildBefore(_dialogXML.replic.(@id == id), comment);
		}
		
		public function getContent(): XML {
			return _dialogXML;
		}
		
		public function setDialogXmlFromString(stringToXml: String): void {
			var editorText:XML = XML(stringToXml);
			_dialogXML = editorText;
		}
		
		public function setDialogXml(xml: XML): void {
			_dialogXML = xml;
		}
		
		public function tryToSetDialogXml(stringToXml: String): Boolean {
			try {
				setDialogXmlFromString(stringToXml);
				return true;
			} catch (e:Error) {
				return false;
			}
			return false;
		}
		
		public function GetReplicsWithMultiplePointsTo():Array {
			var replics:Array = new Array();
			for each(var replic:XML in _dialogXML.replic){
				if (replic.pointsTo.replicNode.length() > 1){
					//trace(GetReplicTextById(replic.pointsTo.replicNode.@id));
					replics.push({data:replic.@id, label:replic.@id + ": "+replic.text})
				}
			}
			return replics;
		}
		
		public function GetPointsToIdsFrom(id:String):Array {
			var ids:Array = new Array();
			var replic:XML = getReplicAsXMLById(id);
			for each(var replicNode:XML in replic.pointsTo.replicNode){
				//trace(replicNode.@id);
				ids.push({data:replicNode.@id, label:replicNode.@id + ": "+GetReplicTextById(replicNode.@id)})
			}
			return ids;
		}
	}
}