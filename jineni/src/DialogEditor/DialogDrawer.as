package DialogEditor 
{
	import DialogEditor.DialogObject.Dialog;
	import DialogEditor.DialogObject.Replic;
	import DialogEditor.Utils.StringConst;
	import DialogEditor.Utils.Colors;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	public class DialogDrawer extends Sprite
	{
		private var _dialog:Dialog;
		private var _alreadyDrownReplics:Array = new Array();
		private var _gridStepX:int = 0;
		private var _gridStepY:int = 0;
		private var _redundantReplics:Array /* of Strings */ = new Array();
		
		public static const DEBUG:Boolean = false;
		public static const GRAPH_ORIGIN_X:int = 10;
		public static const GRAPH_ORIGIN_Y:int = 10;
		public static const GRAPH_GRID_X_PADDING:int = 50;
		public static const GRAPH_GRID_Y_PADDING:int = 50;
		
		public function DialogDrawer(dialog: Dialog)
		{
			super();
			this._dialog = dialog;
		}
		
		public function drawDialog():void
		{		
			//TODO check if replic names in corect reg exp
			_alreadyDrownReplics = new Array();
			_gridStepX = 0;
			_gridStepY = 0;
			_redundantReplics = _dialog.getAllReplics();
			var entryPoints:XMLList = this._dialog.findEntryPointsInXML();
			var entryPoint:XML = null;
			//trace("entryPoints="+entryPoints.length());
			for each (entryPoint in entryPoints)
			{
				extractAndHandleReplicFromXmlById(entryPoint.@id);
			}
			
			//add blank to better scroll
			var rectangle:Shape = new Shape; 
			rectangle.graphics.beginFill(0xFF0000); 
			var x:int = GRAPH_ORIGIN_X + (_gridStepX + 4) * GRAPH_GRID_X_PADDING;
			var y:int = GRAPH_ORIGIN_Y + (_gridStepY + 4) * GRAPH_GRID_Y_PADDING;
			rectangle.graphics.drawRect(x, y+100, 1,1); 
			rectangle.graphics.endFill();
			this.addChild(rectangle);

			//trace("drawing dialog started");
			getDialogVisualization();
			//trace("drawing dialog finnished");
			checkRedundantReplics();
		}
		
		private function extractAndHandleReplicFromXmlById(id:String):Replic
		{
			var replic:Replic = createReplicFromXmlById(id);
			isNotRedundantReplic(id);
			var nextCount:int = 0;
			var successor:Replic;
			
			for each (var nextReplic:XML in replic.getNext().elements(StringConst.REPLIC_NODE))
			{
				successor = extractAndHandleReplicFromXmlById(nextReplic.@[StringConst.ID]);
				replic.addSuccessor(successor);
				//is it here for reason??
				if (successor.isEntryPoint())
				{
					//trace("adding replic from successor.isEntryPoint()"+this._dialog.getEntryPoints().length);
					this._dialog.addEntryPoint(replic);
				}
			}

			this._dialog.addReplic(replic);
			if (replic.isEntryPoint())
			{
				//_gridStepY++;
				//trace("adding replic from replic.isEntryPoint()"+this._dialog.getEntryPoints().length);
				this._dialog.addEntryPoint(replic);
				
			}
			return replic;
		}
		
		private function isNotRedundantReplic(id:String):void
		{
			var index:int = _redundantReplics.indexOf(id);			
			if (index === -1)
			{
				//might be already removed from redundants
			}
			else
			{
				this._redundantReplics.removeAt(index);
			}
		}
		
		private function createReplicFromXmlById(id:String):Replic
		{
			return this._dialog.createReplicFromXMLById(id);
		}
		
		private function deleteReplicFromXmlById(id:String):void
		{
			this._dialog.deleteReplicFromXmlById(id);
		}
		
		public function getDialogVisualization():void
		{
			for each (var entryPointReplic:Replic in this._dialog.getEntryPoints())
			{
				drawNode(entryPointReplic, GRAPH_ORIGIN_X, GRAPH_ORIGIN_Y + this._gridStepY * GRAPH_GRID_Y_PADDING);
			}
		}
		
		private function drawNode(replic:Replic, x:int, y:int):Replic
		{
			//trace(replic, x, y);
			var parentNode:Replic = replic;
			parentNode.x = x
			parentNode.y = y;
			this._gridStepX++;
			this.addChild(parentNode);
			var i:int;
			var successors:Array = replic.getSuccessors();
			var successorsCount:int = successors.length;
			var yPos:int = 0;
			var successorNode:Replic;
			var isAlreadyDrown:*;
			if (replic.dialogContinues())
			{
				for (i = 0; i < successorsCount; i++)
				{
					isAlreadyDrown = getAlreadyDrawnReplic(successors[i].getId());
					if (isAlreadyDrown === false)
					{
						//!! this._gridStepY * GRAPH_GRID_Y_PADDING + GRAPH_ORIGIN_Y (JE POTREBA CHECKOVAT PRES _gridStepY JE TO GLOBALNI HODNOTA, TAKZE MA PREHLED O TOM CO SE DEJE, FAKT!!!!
						successorNode = drawNode(successors[i], x + parentNode.width + GRAPH_GRID_X_PADDING, this._gridStepY * GRAPH_GRID_Y_PADDING + GRAPH_ORIGIN_Y);
						addAlreadyDrownReplic(successors[i]);
					}
					else
					{
						successorNode = isAlreadyDrown;
					}
					connectNodes(parentNode, successorNode);
				}
			}
			else
			{
				drawEndDialogNode(parentNode);
				//trace("incrementing _gridStepY after "+parentNode.getId()+" to "+this._gridStepY);
				this._gridStepY++;
			}
			return parentNode;
		}
		
		private function connectNodes(startNode:Replic, endNode:Replic):void
		{
			var line:Shape = new Shape();
			var lineColor:uint;
			var lineAlpha:Number;
			if (startNode.isRandomPointer()) {
				lineAlpha = 0.5;
				lineColor = Colors.LINE_COLOR_RANDOM;
			} else {
				lineAlpha = 1;
				lineColor = Colors.LINE_COLOR_POINTS_TO;
			}
			line.graphics.lineStyle(2, lineColor, lineAlpha);
			line.graphics.moveTo(startNode.x + startNode.width, startNode.y + 10);
			line.graphics.lineTo(endNode.x, endNode.y + 10)
			this.addChild(line);
		}
		
		private function drawEndDialogNode(endNode:Replic):void
		{
			var line:Shape = new Shape();
			line.graphics.lineStyle(2, Colors.LINE_COLOR_POINTS_TO);
			line.graphics.moveTo(endNode.x + endNode.width, endNode.y + 10);
			line.graphics.lineTo(endNode.x + endNode.width + 30, endNode.y + 10)
			this.addChild(line);
		}
		
		private function getAlreadyDrawnReplic(id:String):*
		{
			for each (var replic:Replic in this._alreadyDrownReplics)
			{
				if (replic.getId() === id)
				{
					return replic;
				}
			}
			return false;
		}
		
		private function addAlreadyDrownReplic(replic:Replic):void
		{
			this._alreadyDrownReplics.push(replic);
		}
		
		private function checkRedundantReplics():void
		{
			//trace("redundant replics="+this._redundantReplics);
		}
		
		public function getRedundantReplicNames():Array 
		{
			return _redundantReplics;
		}
		/*
		private function replicUsedInDialog(replicId:String):void {
			if (!_dialog.replicExists(replicId)) {
				throw new RuntimeInternalError("nelze zaznamenat pouziti repliky id=" + replicId+", neexistuje");
			}
		}*/
	}
}