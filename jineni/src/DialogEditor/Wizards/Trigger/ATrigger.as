package DialogEditor.Wizards.Trigger 
{
	import Common.Controller.WizardController;
	import DialogEditor.Wizards.AddTriggerWizzard;
	import fl.controls.Button;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class ATrigger extends WizardController
	{
		protected var _addTriggerWizzard:AddTriggerWizzard;
		protected var _resetDataInNextOpen:Boolean;
		
		public function ATrigger(parentController: WizardController, sprite:Sprite) 
		{
			super(parentController, sprite);
			_addTriggerWizzard = _parentController as AddTriggerWizzard;
			_resetDataInNextOpen = true;
		}
		
		public function ResetData():void {
			_resetDataInNextOpen = true;
		}
		
		public function Open():void {
			trace("open");
		}		
		
		public function close(evnt:MouseEvent):void {
			trace("close");
		}
		
		public function attach(evnt:MouseEvent):void {
			trace("attach");
		}
		
		public function GetGeneratedXML():XML {
			trace("preview");
			return new XML();
		}
	}
}