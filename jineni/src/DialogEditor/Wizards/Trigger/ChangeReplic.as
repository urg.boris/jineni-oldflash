package DialogEditor.Wizards.Trigger 
{
	import Common.Controller.WizardController;
	import DialogEditor.Exception.IncorectInput;
	import DialogEditor.Wizards.AddTriggerWizzard;
	import fl.controls.Button;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import fl.controls.List;
	import flash.events.Event;
	import DialogEditor.Utils.StringConst;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class ChangeReplic extends ATrigger
	{
		protected var _replicsData:DataProvider;
		protected var _replicNodesData:DataProvider;
		protected var _statesData:DataProvider;
		protected var _replics:List;
		protected var _replicNodes:List;
		protected var _states:List;		
		
		public function ChangeReplic(parentController: AddTriggerWizzard, sprite:Sprite) 
		{
			_eventHandlersData = new Array(
				{spriteName: "possibleReplics", eventHandlerName: "onChangeReplic", type: List, eventType: Event.CHANGE}
			);
			super(parentController, sprite);
			
			_replics = GetObjectInLoadedGraphic("possibleReplics", List);
			_replicNodes = GetObjectInLoadedGraphic("possibleReplicNodes", List);
			_states = GetObjectInLoadedGraphic("states", List);
		}
		
		override public function Open():void {
			if (_resetDataInNextOpen) {
				_resetDataInNextOpen = false;
				
				_replicsData = new DataProvider(_addTriggerWizzard.GetReplicsWithMultiplePointsTo());
				_replics.dataProvider = _replicsData;
				
				_statesData = new DataProvider(new Array({data:StringConst.T_R_ACTIVE, label:StringConst.T_R_ACTIVE}, {data:StringConst.T_R_INACTIVE, label:StringConst.T_R_INACTIVE}));
				_states.dataProvider = _statesData;
			} 
			OpenWizzard();
		}
		
		override public function ResetData():void {
			_replicNodes.dataProvider = new DataProvider();
			super.ResetData();
		}
		
		override public function GetGeneratedXML():XML {
			if (_replics.selectedIndex == -1 || _replicNodes.selectedIndex == -1 || _states.selectedIndex == -1) {
				throw new IncorectInput("to generate XML select all boxes");
			}
			return new XML(
				"<" + StringConst.T_R_CHANGE_POINTSTO + " " +
					StringConst.T_R_FOR_REPLIC + "=\"" + _replics.selectedItem.data + "\" " +
					StringConst.T_R_REPLIC_NODE_ID + "=\"" + _replicNodes.selectedItem.data + "\" " +
					StringConst.T_R_SET_STATE_TO + "=\"" + _states.selectedItem.data  + "\"/>"
				);
		}
		
		public function onChangeReplic(event:Event):void {
			_replicNodesData = new DataProvider(_addTriggerWizzard.GetPointsToIdsFrom(event.target.selectedItem.data));
			_replicNodes.dataProvider = _replicNodesData;
		}
	}
}