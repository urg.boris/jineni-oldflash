package DialogEditor.Wizards.Trigger 
{
	import Common.Controller.WizardController;
	import DialogEditor.Exception.IncorectInput;
	import DialogEditor.Wizards.AddTriggerWizzard;
	import fl.controls.Button;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import fl.controls.List;
	import flash.events.Event;
	import DialogEditor.Utils.StringConst;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class ChangeItem extends ATrigger
	{
		protected var _itemsData:DataProvider;
		protected var _actionsData:DataProvider;
		protected var _conditionsData:DataProvider;
		protected var _items:List;
		protected var _actions:List;
		protected var _conditions:List;		
		
		public function ChangeItem(parentController: AddTriggerWizzard, sprite:Sprite) 
		{
			super(parentController, sprite);
			
			_items = GetObjectInLoadedGraphic("items", List);
			_actions = GetObjectInLoadedGraphic("actions", List);
			_conditions = GetObjectInLoadedGraphic("conditions", List);
		}
		
		override public function Open():void {
			if (_resetDataInNextOpen) {
				_resetDataInNextOpen = false;
				_itemsData = _addTriggerWizzard.GetItemsData();
				_items.dataProvider = _itemsData;

				_actionsData = new DataProvider(new Array(
					{data:StringConst.T_I_A_GAIN, label:StringConst.T_I_A_GAIN}, 
					{data:StringConst.T_I_A_LOOSE, label:StringConst.T_I_A_LOOSE},
					{data:StringConst.T_I_A_USE, label:StringConst.T_I_A_USE},
					{data:StringConst.T_I_A_HAVE, label:StringConst.T_I_A_HAVE},
					{data:StringConst.T_I_A_HAVE_NOT, label:StringConst.T_I_A_HAVE_NOT},
					{data:StringConst.T_RENAME, label:StringConst.T_RENAME}
				));
				_actions.dataProvider = _actionsData;
				
				_conditionsData = new DataProvider(new Array(
					{data:StringConst.T_C_NONE, label:StringConst.T_C_NONE}, 
					{data:StringConst.T_I_C_HAVE, label:StringConst.T_I_C_HAVE}, 
					{data:StringConst.T_I_C_HAVE_NOT, label:StringConst.T_I_C_HAVE_NOT}
				));
				_conditions.dataProvider = _conditionsData;
			} 
			OpenWizzard();
		}
		
		override public function ResetData():void {
			//TODO set index to 0, mozna?
		}
		
		override public function GetGeneratedXML():XML {
			if (_items.selectedIndex == -1 || _actions.selectedIndex == -1 || _conditions.selectedIndex == -1) {
				throw new IncorectInput("to generate XML select all boxes");
			}
			var condition:String = _conditions.selectedIndex == 0 ? "" : " " + StringConst.T_CONDITION + "=\"" + _conditions.selectedItem.data + "\"";
			return new XML(
				"<" + StringConst.T_I_CHANGE_ITEM + " " +
					StringConst.ID + "=\"" + _items.selectedItem.data + "\" " +
					StringConst.T_ACTION + "=\"" + _actions.selectedItem.data  + "\"" +
					condition +" />"
				);
		}
		
	}
}