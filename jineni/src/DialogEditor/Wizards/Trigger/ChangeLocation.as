package DialogEditor.Wizards.Trigger 
{
	import Common.Controller.WizardController;
	import DialogEditor.Exception.IncorectInput;
	import DialogEditor.Wizards.AddTriggerWizzard;
	import fl.controls.Button;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import fl.controls.List;
	import flash.events.Event;
	import DialogEditor.Utils.StringConst;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class ChangeLocation extends ATrigger
	{
		protected var _locationsData:DataProvider;
		protected var _actionsData:DataProvider;
		protected var _conditionsData:DataProvider;
		protected var _locations:List;
		protected var _actions:List;
		protected var _conditions:List;
		

		public function ChangeLocation(parentController: AddTriggerWizzard, sprite:Sprite) 
		{
			super(parentController, sprite);			
			_locations = GetObjectInLoadedGraphic("locations", List);
			_actions = GetObjectInLoadedGraphic("actions", List);
			_conditions = GetObjectInLoadedGraphic("conditions", List);
		}
		
		override public function Open():void {
			if (_resetDataInNextOpen) {
				_resetDataInNextOpen = false;
				_locationsData = _addTriggerWizzard.GetLocationsData();
				_locations.dataProvider = _locationsData;

				_actionsData = new DataProvider(new Array(
					{data:StringConst.T_L_A_DISCOVER, label:StringConst.T_L_A_DISCOVER}, 
					{data:StringConst.T_L_A_FORGET, label:StringConst.T_L_A_FORGET},
					{data:StringConst.T_L_A_IS_OPENED, label:StringConst.T_L_A_IS_OPENED},
					{data:StringConst.T_L_A_IS_CLOSED, label:StringConst.T_L_A_IS_CLOSED},
					{data:StringConst.T_L_A_MAKE_DISCOVERABLE, label:StringConst.T_L_A_MAKE_DISCOVERABLE}
				));
				_actions.dataProvider = _actionsData;
				
				_conditionsData = new DataProvider(new Array(
					{data:StringConst.T_C_NONE, label:StringConst.T_C_NONE}, 
					{data:StringConst.T_L_C_IF_OPENED, label:StringConst.T_L_C_IF_OPENED}, 
					{data:StringConst.T_L_C_IF_OPENED_NOT, label:StringConst.T_L_C_IF_OPENED_NOT},
					{data:StringConst.T_L_C_IF_ON_MAP, label:StringConst.T_L_C_IF_ON_MAP},
					{data:StringConst.T_L_C_IF_ON_MAP_NOT, label:StringConst.T_L_C_IF_ON_MAP_NOT}
				));
				_conditions.dataProvider = _conditionsData;
			} 
			OpenWizzard();
		}
		
		override public function ResetData():void {
			//TODO set index to 0, mozna?
		}
		
		override public function GetGeneratedXML():XML {
			if (_locations.selectedIndex == -1 || _actions.selectedIndex == -1 || _conditions.selectedIndex == -1) {
				throw new IncorectInput("to generate XML select all boxes");
			}
			
			var condition:String = _conditions.selectedIndex == 0 ? "" : " " + StringConst.T_CONDITION + "=\"" + _conditions.selectedItem.data + "\"";

			return new XML(
				"<" + StringConst.T_I_CHANGE_LOCATION + " " +
					StringConst.ID + "=\"" + _locations.selectedItem.data + "\" " +
					StringConst.T_ACTION + "=\"" + _actions.selectedItem.data  + "\"" +
					condition +"/>"
				);
		}
		
	}
}