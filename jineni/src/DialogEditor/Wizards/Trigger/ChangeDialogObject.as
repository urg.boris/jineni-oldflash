package DialogEditor.Wizards.Trigger 
{
	import Common.Controller.WizardController;
	import DialogEditor.Exception.IncorectInput;
	import DialogEditor.Wizards.AddTriggerWizzard;
	import fl.controls.Button;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import fl.controls.List;
	import flash.events.Event;
	import DialogEditor.Utils.StringConst;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class ChangeDialogObject extends ATrigger
	{
		protected var _dialogObjectsData:DataProvider;
		protected var _actionsData:DataProvider;
		protected var _dialogObjects:List;
		protected var _actions:List;
		protected var _toValue:TextField;

		public function ChangeDialogObject(parentController: AddTriggerWizzard, sprite:Sprite) 
		{
			super(parentController, sprite);			
			_dialogObjects = GetObjectInLoadedGraphic("dialogObjects", List);
			_actions = GetObjectInLoadedGraphic("actions", List);
			_toValue = GetObjectInLoadedGraphic("toValue", TextField);
		}
		
		override public function Open():void {
			if (_resetDataInNextOpen) {
				_resetDataInNextOpen = false;
				_dialogObjectsData = _addTriggerWizzard.GetDialogObjectsData();
				_dialogObjects.dataProvider = _dialogObjectsData;

				_actionsData = new DataProvider(new Array(
					{data:StringConst.T_D_A_SET_ENTRY_POINT, label:StringConst.T_D_A_SET_ENTRY_POINT}, 	
					{data:StringConst.T_D_A_SET_ACTUAL_ANIMATION, label:StringConst.T_D_A_SET_ACTUAL_ANIMATION}, 
					{data:StringConst.T_D_A_SET_IDLE_ANIMATION, label:StringConst.T_D_A_SET_IDLE_ANIMATION}, 
					//TODO
					{data:StringConst.T_RENAME, label:StringConst.T_RENAME}, 
					{data:StringConst.T_D_A_SHOW, label:StringConst.T_D_A_SHOW}, 
					{data:StringConst.T_D_A_HIDE, label:StringConst.T_D_A_HIDE},
					{data:StringConst.T_D_A_SHOW_MOUSE_INTERACTIVITY, label:StringConst.T_D_A_SHOW_MOUSE_INTERACTIVITY},
					{data:StringConst.T_D_A_HIDE_MOUSE_INTERACTIVITY, label:StringConst.T_D_A_HIDE_MOUSE_INTERACTIVITY},
					{data:StringConst.T_D_A_MAKE_INTERACTIVE, label:StringConst.T_D_A_MAKE_INTERACTIVE},
					{data:StringConst.T_D_A_MAKE_NONINTERACTIVE, label:StringConst.T_D_A_MAKE_NONINTERACTIVE},
					{data:StringConst.T_D_A_ACTIVE, label:StringConst.T_D_A_ACTIVE},
					{data:StringConst.T_D_A_INACTIVE, label:StringConst.T_D_A_INACTIVE}
				));
				_actions.dataProvider = _actionsData;
				
			} 
			OpenWizzard();
		}
		
		override public function ResetData():void {
			//TODO set index to 0, mozna?
		}
		
		override public function GetGeneratedXML():XML {
			//TODO overeni opt param
			if (_dialogObjects.selectedIndex == -1 || _actions.selectedIndex == -1) {
				throw new IncorectInput("to generate XML select all boxes");
			}
			
			if (!(_actions.selectedIndex == 0 || _actions.selectedIndex == 1 || _actions.selectedIndex == 2)) {
				_toValue.text = "";
			}
			
			var tovalue:String = _toValue.text == "" ? "" : StringConst.T_D_TO_VALUE+"=\"" +_toValue.text + "\" ";
			return new XML(
				"<" + StringConst.T_D_CHANGE_DIALOG_OBJECT + " " +
					StringConst.ID + "=\"" + _dialogObjects.selectedItem.data + "\" " +
					StringConst.T_ACTION + "=\"" + _actions.selectedItem.data  + "\"" +
					tovalue+
					"/>"
				);
		}
		
	}
}