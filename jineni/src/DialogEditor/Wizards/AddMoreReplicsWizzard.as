package DialogEditor.Wizards 
{
	import Common.Controller.WizardController;
	import DialogEditor.DialogEditorController;
	import DialogEditor.Exception.IncorectInput;
	import fl.controls.Button;
	import fl.controls.CheckBox;
	import fl.controls.List;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import Common.Utils.Checker;
	import flash.text.TextField;
	import DialogEditor.Utils.StringConst;;
	import DialogEditor.Utils.EditorTools;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class AddMoreReplicsWizzard extends WizardController
	{
		protected var _dialogEditor:DialogEditorController;
		protected var _actualAndNextReplicId:Array;
		public function AddMoreReplicsWizzard(parentController: DialogEditorController, sprite:Sprite) 
		{
			_eventHandlersData = new Array(
				{spriteName: "close", eventHandlerName: "onClose", type: Button},
				{spriteName: "preview", eventHandlerName: "onPreview", type: Button},
				{spriteName: "attach", eventHandlerName: "onAttach", type: Button}
			);
			super(parentController, sprite);
			_dialogEditor = _parentController as DialogEditorController;
		}

		public function Open(actualAndNextReplicId:Array, text:String, isEntryPoint:Boolean = false):void {
			_actualAndNextReplicId = actualAndNextReplicId;
			
			(_sprite["replicId"] as TextField).text = actualAndNextReplicId[1];
			(_sprite["replicText"] as TextField).text = text;
			(_sprite["isEntryPoint"] as CheckBox).selected = isEntryPoint;
			(_sprite["dialogObject"] as List).dataProvider = _dialogEditor.GetDialogObjectsData();
			(_sprite["dialogObject"] as List).selectedIndex = 0;
			//!! hide initial ReplicId in sprite for later
			/*_sprite["originReplicId"] = actualAndNextReplicId[0];
			_sprite["nextReplicId"] = actualAndNextReplicId[1];*/
			
			var xmlText:XML = EditorTools.createNewReplicXML(actualAndNextReplicId[1], "P1", text, isEntryPoint);			
			(_sprite["replicXml"] as TextField).text = xmlText.toString();
			
			OpenWizzard();
		}
		
		public function onPreview(evnt:MouseEvent):void {
			try {
				var xmlText:XML = prepareReplic();
				(_sprite["replicXml"] as TextField).text = xmlText.toString();
			} catch (e:Error)
			{
				_dialogEditor.ShowInfoMessage(e.message);
			}
		}
		
		public function onClose(evnt:MouseEvent):void {
			Close();
		}
		
		override public function Close():void {
			super.Close();
			_dialogEditor.onCloseWizard();
		}

		public function onAttach(evnt:MouseEvent):void {
			var replicId:String = _actualAndNextReplicId[0];
			var nextReplicId:String = _actualAndNextReplicId[1];
			
			var lastReplic:XML = _dialogEditor.GetReplicAsXMLById(replicId);
			//!!aby se nemazali uz vytvorene odkazy na nasledniky
			trace(lastReplic.pointsTo.children()[0].name());
			if (lastReplic.pointsTo.children()[0].name() == StringConst.DIALOG_END)
			{
				lastReplic.pointsTo = new XML("<pointsTo><replicNode id=\"" + nextReplicId + "\"/></pointsTo>");
			}
			
			var howMuch:int = (int)((_sprite["howMuch"] as TextField).text);
			
			var newReplic:XML = prepareReplic();
			delete newReplic.pointsTo.endDialog;
			for (var i:int = 1; i <= howMuch; i++)
			{
				newReplic.pointsTo.appendChild(XML("<replicNode id=\"" + nextReplicId + String.fromCharCode(i + 64) + StringConst.REPLIC_ID_SEPARATOR + "1\"/>"));
			}
			_dialogEditor.onAttachAddMoreReplicsWizard(_actualAndNextReplicId, newReplic, howMuch);
		}
		
		public function prepareReplic():XML {
			if ((_sprite["dialogObject"] as List).selectedItem == null) {
				throw new IncorectInput("postava nevybrana");
			}
			return EditorTools.createNewReplicXML((_sprite["replicId"] as TextField).text, (_sprite["dialogObject"] as List).selectedItem.data, (_sprite["replicText"] as TextField).text, (_sprite["isEntryPoint"] as CheckBox).selected);			
		}
	}
}