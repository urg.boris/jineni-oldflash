package DialogEditor.Wizards 
{
	import Common.Controller.WizardController;
	import DialogEditor.DialogEditorController;
	import DialogEditor.Exception.IncorectInput;
	import fl.controls.Button;
	import fl.controls.CheckBox;
	import fl.controls.List;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import Common.Utils.Checker;
	import flash.text.TextField;
	import DialogEditor.Utils.StringConst;;
	import DialogEditor.Utils.EditorTools;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class AddReplicWizzard extends WizardController
	{
		protected var _dialogEditor:DialogEditorController;
		public function AddReplicWizzard(parentController: DialogEditorController, sprite:Sprite) 
		{
			_eventHandlersData = new Array(
				{spriteName: "close", eventHandlerName: "onClose", type: Button},
				{spriteName: "preview", eventHandlerName: "onPreview", type: Button},
				{spriteName: "attach", eventHandlerName: "onAttach", type: Button}
			);
			super(parentController, sprite);
			_dialogEditor = _parentController as DialogEditorController;
		}
		
		
		public function Open(actualAndNextReplicId:Array, dialogObject:int, text:String, isEntryPoint:Boolean = false):void {
			
			(_sprite["replicId"] as TextField).text = actualAndNextReplicId[1];
			(_sprite["replicText"] as TextField).text = text;
			(_sprite["isEntryPoint"] as CheckBox).selected = isEntryPoint;
			(_sprite["dialogObject"] as List).dataProvider = _dialogEditor.GetDialogObjectsData();
			(_sprite["dialogObject"] as List).selectedIndex = 0;
			//!! hide initial ReplicId in sprite for later
			_sprite["originReplicId"] = actualAndNextReplicId[0];
			_sprite["nextReplicId"] = actualAndNextReplicId[1];
			
			var xmlText:XML =  EditorTools.createNewReplicXML(actualAndNextReplicId[1], "P1", text, isEntryPoint);			
			(_sprite["replicXml"] as TextField).text = xmlText.toString();
			OpenWizzard();
		}
		
		public function onPreview(evnt:MouseEvent):void {
			try {
				var xmlText:XML = prepareReplic();
				(_sprite["replicXml"] as TextField).text = xmlText.toString();
			} catch (e:Error)
			{
				_dialogEditor.ShowInfoMessage(e.message);
			}
		}
		
		public function onClose(evnt:MouseEvent):void {
			Close();
		}
		
		override public function Close():void {
			super.Close();
			_dialogEditor.onCloseWizard();
		}
		
		public function onAttach(evnt:MouseEvent):void {
			var newReplic:XML = prepareReplic();
			
			if (!(_sprite["isEntryPoint"] as CheckBox).selected) {
				var lastReplic:XML = _dialogEditor.GetReplicAsXMLById(_sprite["originReplicId"]);
				//!!aby se nemazali uz vytvorene odkazy na nasledniky
				if (lastReplic.pointsTo.children()[0].name() == StringConst.DIALOG_END)
				{
					lastReplic.pointsTo = new XML("<pointsTo><replicNode id=\"" + _sprite["nextReplicId"] + "\"/></pointsTo>");
				}
			}
			_dialogEditor.onAttachReplicWizzard(_sprite["originReplicId"], newReplic);
			
		}
		
		public function prepareReplic():XML {
			if ((_sprite["dialogObject"] as List).selectedItem == null) {
				throw new IncorectInput("postava nevybrana");
			}
			return EditorTools.createNewReplicXML((_sprite["replicId"] as TextField).text, (_sprite["dialogObject"] as List).selectedItem.data, (_sprite["replicText"] as TextField).text, (_sprite["isEntryPoint"] as CheckBox).selected);			
		}
	}
}