package DialogEditor.Wizards 
{
	import Common.Controller.WizardController;
	import Common.Exception.RuntimeInternalError;
	import DialogEditor.DialogEditorController;
	import DialogEditor.Wizards.Trigger.ATrigger;
	import DialogEditor.Wizards.Trigger.ChangeDialogObject;
	import DialogEditor.Wizards.Trigger.ChangeItem;
	import DialogEditor.Wizards.Trigger.ChangeLocation;
	import DialogEditor.Wizards.Trigger.ChangeReplic;
	import fl.controls.Button;
	import fl.controls.List;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class AddTriggerWizzard extends WizardController
	{
		protected var _attachToReplicId:String;
		protected var _dialogEditor:DialogEditorController;
		protected var _location:ChangeLocation;
		protected var _dialogObject:ChangeDialogObject;
		protected var _item:ChangeItem;
		protected var _replic:ChangeReplic;
		protected var _triggerTypesData:DataProvider;
		protected var _actualSubWizzard: ATrigger;
		protected var _preview: TextField;
		protected var _replicId:TextField;
		protected var _triggerType:List;
		
		public function AddTriggerWizzard(parentController: DialogEditorController, sprite:Sprite) 
		{
			_eventHandlersData = new Array(
				{spriteName: "close", eventHandlerName: "onClose", type: Button},
				{spriteName: "preview", eventHandlerName: "onPreview", type: Button},
				{spriteName: "attach", eventHandlerName: "attach", type: Button},
				{spriteName: "triggerType", eventHandlerName: "onChangeType", type: List, eventType: Event.CHANGE}
			);
			super(parentController, sprite);
			_preview = GetObjectInLoadedGraphic("previewXML", TextField);
			
			_dialogEditor = _parentController as DialogEditorController;
		
			_triggerTypesData = new DataProvider();
			_triggerTypesData.addItem({data:0,label:"zmena moznosti rozvetveni dialogu"});
			_triggerTypesData.addItem({data:1,label:"zmena predmetu inventare"});
			_triggerTypesData.addItem({data:2,label:"zmena dialog objektu"});
			_triggerTypesData.addItem({data:3,label:"zmena lokace"});

			_triggerType = GetObjectInLoadedGraphic("triggerType", List);
			_triggerType.dataProvider = _triggerTypesData;
			_triggerType.selectedIndex = 0;
			
			_replicId = GetObjectInLoadedGraphic("replicId", TextField);
			
			var subWizzard:Sprite = GetSpriteInLoadedGraphic("changeReplic");
			_replic = new ChangeReplic(this, subWizzard);
			
			subWizzard = GetSpriteInLoadedGraphic("changeItem");
			_item = new ChangeItem(this, subWizzard);
			
			subWizzard = GetSpriteInLoadedGraphic("changeDialogObject");
			_dialogObject = new ChangeDialogObject(this, subWizzard);
			
			subWizzard = GetSpriteInLoadedGraphic("changeLocation");
			_location = new ChangeLocation(this, subWizzard);
		}
		
		public function Open(attachToReplicId:String):void {
			_attachToReplicId = attachToReplicId;
			_replicId.text = attachToReplicId;
			OpenWizzard();
			_triggerType.selectedItem = _triggerTypesData.getItemAt(0);
			_actualSubWizzard = _replic;
			_replic.Open();
		}		
		
		public function onClose(evnt:MouseEvent):void {
			Close();
		}
		
		override public function Close():void {
			_actualSubWizzard.RemoveListeners();
			_actualSubWizzard.Hide();
			_replic.ResetData();
			//TODO
			super.Close();
			_dialogEditor.onCloseWizard();
		}
		
		public function attach(evnt:MouseEvent):void {
			try {
				_dialogEditor.onAttachTrigger(_attachToReplicId, _actualSubWizzard.GetGeneratedXML());
			} catch (e:Error)
			{
				_dialogEditor.ShowInfoMessage(e.message);
			}
			
			
		}
		public function onPreview(evnt:MouseEvent):void {
			try {
				_preview.text = _actualSubWizzard.GetGeneratedXML().toXMLString();
			} catch (e:Error)
			{
				_dialogEditor.ShowInfoMessage(e.message);
			}
		}
		
		private function SwitchActualSubWizzardTo(subWizzard:ATrigger):void {
			_actualSubWizzard.RemoveListeners();
			_actualSubWizzard.Hide();
			
			_actualSubWizzard = subWizzard;
			_actualSubWizzard.Open();
		}
		
		public function onChangeType(event:Event):void
		{
			switch (event.target.selectedItem.data)
			{
				case 0: 
					SwitchActualSubWizzardTo(_replic);
					break;
				case 1: 
					SwitchActualSubWizzardTo(_item);
					break;
				case 2: 
					SwitchActualSubWizzardTo(_dialogObject);
					break;
				case 3: 
					SwitchActualSubWizzardTo(_location);
					break;
				default:
					throw new RuntimeInternalError("unknown switch option error");
					break;
			}
		}
		
		public function GetReplicsWithMultiplePointsTo():Array {
			return _dialogEditor.GetReplicsWithMultiplePointsTo();
		}
		
		public function GetPointsToIdsFrom(id:String):Array {
			return _dialogEditor.GetPointsToIdsFrom(id);
		}
		
		public function GetItemsData(): DataProvider {
			return _dialogEditor.GetItemsData();
		}
		
		public function GetLocationsData(): DataProvider {
			return _dialogEditor.GetLocationsData();
		}
		
		public function GetDialogObjectsData(): DataProvider {
			return _dialogEditor.GetDialogObjectsData();
		}
	}
}