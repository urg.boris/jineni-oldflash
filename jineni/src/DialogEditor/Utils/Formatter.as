package DialogEditor.Utils 
{
	/**
	 * ...
	 * @author Plesnif
	 */
	public class Formatter 
	{
		
		public static const COMENT_COLOR:uint = 0xFF0000;
		public static const REPLIC_COLOR:uint = 0x01bb01;
		public static const TEXT_COLOR:uint = 0x0635DD;
		public static const POINTS_TO_COLOR:uint = 0xec891d;
		public static const TRIGGER_COLOR:uint = 0xa04995;
		public static const ENDDIALOG_COLOR:uint = 0xdd1111;
		
		public static function HightlightAll(text:String):String {
			text = PrepareStringAsHtmlString(text);
			text = HighlightComments(text);
			text = HighlightReplic(text);
			text = HighlightText(text);
			text = HighlighPointsTo(text);
			text = HighlighTrigger(text);
			text = HighlighEndDialog(text);	
			return text;
		}
		
		public static function PrepareStringAsHtmlString(text:String):String {
			text = text.replace(/</g, "&lt;"); 
			text = text.replace(/>/g, "&gt;");
			return text;
		}
		
		private static function HighlightElementType(replicStart: RegExp, replaceStartWith: String, replicEnd: RegExp, replaceEndWith: String, inText:String): String {
			inText = inText.replace(replicStart, replaceStartWith); 
			inText = inText.replace(replicEnd, replaceEndWith);
			return inText;
			
		}
		
		public static function HighlightComments(text:String):String {
			var startRegExp:RegExp = /&lt;!--/g;
			var endRegExp:RegExp = /--&gt;/g;
			
			var HtmlCommentStart:String = "<font color=\""+toHexString(COMENT_COLOR)+"\"><strong>&lt;!--";
			var HtmlCommentEnd:String = "--&gt;</strong></font>";
			
			return HighlightElementType(startRegExp, HtmlCommentStart, endRegExp, HtmlCommentEnd, text);
		}
		
		public static function HighlightReplic(text:String):String {
			var replicStart:RegExp = new RegExp("&lt;replic ", "g");//!!mezera nutna
			var replicEnd:RegExp = new RegExp("&lt;\/replic&gt;", "g");

			var replaceStartWith: String = "<font color=\""+ toHexString(REPLIC_COLOR) +"\">&lt;<strong>replic</strong></font> "; //!!mezera nutna
			var replaceEndWith: String = "<font color=\"" + toHexString(REPLIC_COLOR) + "\">&lt;\/<strong>replic&gt;</strong></font>";
			return HighlightElementType(replicStart, replaceStartWith, replicEnd, replaceEndWith, text);
		}
		
		public static function HighlightText(text:String):String {
			//TODO celej modrej???

			var start:RegExp = new RegExp("&lt;text&gt;", "g");
			var end:RegExp = new RegExp("&lt;/text&gt;", "g");

			var replaceStartWith: String = "<font color=\"" + toHexString(TEXT_COLOR) +"\">&lt;text&gt;</strong></font>"; 
			var replaceEndWith: String = "<font color=\"" + toHexString(TEXT_COLOR) +"\">&lt;/text&gt;</strong></font>";
			return HighlightElementType(start, replaceStartWith, end, replaceEndWith, text);
		}
		
		public static function HighlighPointsTo(text:String):String {
			var start:RegExp = new RegExp("&lt;pointsTo&gt;", "g");
			var end:RegExp = new RegExp("&lt;\/pointsTo&gt;", "g");
			
			var replaceStartWith: String = "<font color=\""+ toHexString(POINTS_TO_COLOR) +"\">&lt;<strong>pointsTo&gt;</strong></font>"; 
			var replaceEndWith: String = "<font color=\"" + toHexString(POINTS_TO_COLOR) + "\">&lt;\/<strong>pointsTo&gt;</strong></font>";
			
			return HighlightElementType(start, replaceStartWith, end, replaceEndWith, text);
		}
		
		public static function HighlighTrigger(text:String):String {
			var start:RegExp = new RegExp("&lt;trigger&gt;", "g");
			var end:RegExp = new RegExp("&lt;\/trigger&gt;", "g");
			
			var replaceStartWith: String = "<font color=\""+ toHexString(TRIGGER_COLOR) +"\">&lt;<strong>trigger&gt;</strong></font>"; 
			var replaceEndWith: String = "<font color=\"" + toHexString(TRIGGER_COLOR) + "\">&lt;\/<strong>trigger&gt;</strong></font>";
			
			return HighlightElementType(start, replaceStartWith, end, replaceEndWith, text);
		}
		
		public static function HighlighEndDialog(text:String):String {
			var start:RegExp = new RegExp("&lt;"+StringConst.DIALOG_END+"/&gt;", "g");
			var replaceStartWith: String = "<font color=\""+ toHexString(ENDDIALOG_COLOR) +"\">&lt;<strong>"+StringConst.DIALOG_END+"/&gt;</strong></font>"; 
			return text.replace(start, replaceStartWith);
		}
		

		public static function toHexString(hex:uint):String {
			return "#" + hex.toString(16);
		}
		
	}

}