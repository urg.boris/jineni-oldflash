package DialogEditor.Utils 
{
	/**
	 * ...
	 * @author Plesnif
	 */
	public class StringConst 
	{
		public static const IS_ENTRY_POINT:String = "isEntryPoint";
		public static const IS_RANDOM_POINTER:String = "isRandomPointer";
		public static const DIALOG_OBJECT:String = "dialogObject";
		public static const ID:String = "id";
		public static const POINTS_TO:String = "pointsTo";
		public static const REPLIC_NODE:String = "replicNode";
		public static const TEXT:String = "text";
		public static const TRIGGER:String = "trigger";
		public static const DIALOG_END:String = "dialogEnd";
		public static const LOCATION:String = "location";
		public static const OBJECT:String = "object";
		public static const ITEM:String = "item";
		public static const NAME:String = "name";
		public static const REPLIC_ID_SEPARATOR:String = "_";
		public static const UNFILLED:String = "NEVYPLNENO";
		public static const SEARCH_TAG:String = "qwgwq";
		public static const SEARCH_REPLIC_ID:String = "<replic id=";
		public static const T_ACTION:String = "action";
		public static const T_CONDITION:String = "condition";
		public static const T_DEFALT_NAME:String = "defaultni jmeno";
		public static const T_RENAME:String = "zmena jmena";
		public static const T_C_NONE:String = "-žádná-";
		public static const T_R_CHANGE_POINTSTO:String = "changePointsTo";
		public static const T_R_FOR_REPLIC:String = "forReplic";
		public static const T_R_REPLIC_NODE_ID:String = "replicNodeId";
		public static const T_R_SET_STATE_TO:String = "setStateTo";
		public static const T_R_ACTIVE:String = "active";
		public static const T_R_INACTIVE:String = "inactive";
		public static const T_I_CHANGE_ITEM:String = "changeItem";
		public static const T_I_A_GAIN:String = "gain";
		public static const T_I_A_LOOSE:String = "loose";
		public static const T_I_A_USE:String = "use";
		public static const T_I_A_HAVE:String = "have";
		public static const T_I_A_HAVE_NOT:String = "haveNot";
		public static const T_I_C_HAVE:String = "have";
		public static const T_I_C_HAVE_NOT:String = "haveNot";
		public static const T_I_CHANGE_LOCATION:String = "changeLocation";
		public static const T_L_A_DISCOVER:String = "discover"
		public static const T_L_A_FORGET:String = "forget";
		public static const T_L_A_IS_OPENED:String = "isOpened";
		public static const T_L_A_IS_CLOSED:String = "isClosed";
		public static const T_L_A_MAKE_DISCOVERABLE:String = "makeDiscoverable";
		public static const T_L_C_IF_OPENED:String = "ifOpened";
		public static const T_L_C_IF_OPENED_NOT:String = "ifOpenedNot";
		public static const T_L_C_IF_ON_MAP:String = "ifOnMap";
		public static const T_L_C_IF_ON_MAP_NOT:String = "ifOnMapNot";
		public static const T_D_CHANGE_DIALOG_OBJECT:String = "changeDialogObject";
		public static const T_D_TO_VALUE:String = "toValue";
		public static const T_D_A_SET_ENTRY_POINT:String = "setEntryPoint";
		public static const T_D_A_SET_IDLE_ANIMATION:String = "setIdleAnimation";
		public static const T_D_A_SET_ACTUAL_ANIMATION:String = "setAtualAnimation";
		public static const T_D_A_SHOW:String = "show";
		public static const T_D_A_HIDE:String = "hide";
		public static const T_D_A_SHOW_MOUSE_INTERACTIVITY:String = "showMouseInteractivity";
		public static const T_D_A_HIDE_MOUSE_INTERACTIVITY:String = "hideMouseInteractivity";
		public static const T_D_A_MAKE_INTERACTIVE:String = "makeInteractive";
		public static const T_D_A_MAKE_NONINTERACTIVE:String = "makeNonInteractive";
		public static const T_D_A_ACTIVE:String = "active";
		public static const T_D_A_INACTIVE:String = "inactive";		
	}

}