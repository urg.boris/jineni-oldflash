package DialogEditor.Utils 
{
	import Common.Utils.Checker;
	import DialogEditor.Exception.IncorectInput;
	/**
	 * ...
	 * @author Plesnif
	 */
	public class EditorTools 
	{
		
		public function EditorTools() 
		{
			
		}
		
		public static function createNewReplicXML(replicId:String, characterId:String, text:String, isEntryPoint:Boolean = false):XML
		{
			if (!Checker.isValidReplicId(replicId))
			{
				throw new IncorectInput("replika " + replicId + " nesplnuje pozadavky jmen repliky: " + Checker.validReplicId.toString());
			}
			
			if (!Checker.isValidCharacterId(characterId))
			{
				throw new IncorectInput("id postavy " + characterId + " nesplnuje pozadavky: " + Checker.validCharacterId.toString());
			}
			
			var emptyReplic:XML = new XML("<replic id=\"" + replicId + "\" character=\"" + characterId + "\" "+(isEntryPoint ? " isEntryPoint=\"1\"" : "")+"><text>" + text + "</text><pointsTo><endDialog/></pointsTo></replic>");
			return emptyReplic;
		}
		
	}

}