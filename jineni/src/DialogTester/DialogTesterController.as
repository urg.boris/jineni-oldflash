package DialogTester 
{
	//import Common.Controller.BaseController;
	import Common.Controller.ControllerWithFilesLoader;
	import Common.Dialog.DialogObject.DialogFlow;
	import Common.Exception.RuntimeInternalError;
	import Common.Controller.IController;
	import fl.controls.List;
	import fl.data.DataProvider;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import br.com.stimuli.loading.BulkProgressEvent;
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.loadingtypes.LoadingItem;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import flash.filesystem.FileMode;
	import flash.events.MouseEvent;	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.ErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.errors.IOError;	
	import fl.controls.Button;
	import Common.World.CoreGameObjects;
	import flash.net.registerClassAlias;
	import Common.Utils.GeneralTools;

	public class DialogTesterController extends ControllerWithFilesLoader implements IController
	{		
		private var _mainEventHandlerData: Array;
		private var _fileRef:FileReference;
		private var _infoMessageField:TextField;
		private var _allDialogsData:DataProvider;
		private var _entryPointsData:DataProvider;
		private var _allReplicsData:DataProvider;
		private var _allDialogs:List;
		private var _entryPoints:List;
		private var _allReplics:List;
		private var _emptyAllDialogs: DataProvider = new DataProvider(new Array({label:"------------------------------------->"}));
		private var _dialogFlow: DialogFlow;
		
		public function DialogTesterController(main:Main) {
			_mainEventHandlerData = new Array(
				{spriteName: "loadFile", eventHandlerName: "onLoadFile", type: Button}
				);
			var loader:BulkLoader = new BulkLoader("tester");
			super(main, loader);
			
		}
		
		override protected function ConfigXMLSettings():void {
			XML.ignoreComments = true;
			XML.ignoreProcessingInstructions = true;
		}
		
		public function run():void
		{
			ConfigXMLSettings();			
			//AddCoreXmlFiles();	
			_loader.add("xml/dialog/dialog.xml", {id: "dialog", type: BulkLoader.TYPE_XML});
			_loader.add("graphic/DialogTester.swf", {id: "graphic", type:BulkLoader.TYPE_MOVIECLIP});		
			RunLoader();
		}
		
		override protected function onAllLoaded(evt:BulkProgressEvent):void
		{
			var loadedSWF: Sprite = evt.target.getMovieClip("graphic") as Sprite;
			SetSprite(loadedSWF);
			
			_infoMessageField = GetObjectInLoadedGraphic("infoMessage", TextField);
			
			_allDialogs = (_sprite["allDialogs"] as List);
			_entryPoints = (_sprite["entryPoints"] as List);
			_allReplics = (_sprite["allReplics"] as List);
			
			
			SetEventManagement(loadedSWF, _mainEventHandlerData, false);
			
			trace("DialogTesterController loaded");
			this._main.addChild(loadedSWF);
			
			//pro testovani
			var dialogXML:XML = evt.target.getXML("dialog") as XML;
			simulateOnFileLoadComplete(dialogXML);
		}
		
		public function ShowInfoMessage(message:String):void
		{
			_infoMessageField.text = message;
		}
				
		public function onLoadFile(evt:MouseEvent):void  // =null
		{
			_fileRef = new FileReference();
			_fileRef.addEventListener(Event.SELECT, onFileLoadSelected);
			_fileRef.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			var textTypeFilter:FileFilter = new FileFilter("Dialogové soubory", "*.xml");
			
			_fileRef.browse([textTypeFilter]);
			_allDialogs.dataProvider = _emptyAllDialogs;
		}
		
		protected function onIOError(evt:IOErrorEvent):void
		{
			trace("There was an IO Error.");
		}
		
		private function onFileLoadSelected(evt:Event):void
		{
			_fileRef.addEventListener(Event.COMPLETE, onFileLoadComplete);
			_fileRef.load();
		}
		
		private function onFileLoadComplete(evt:Event):void
		{
			ShowInfoMessage("file " + _fileRef.name + " loaded"+_fileRef.data);
			_dialogFlow = new DialogFlow(new XML(_fileRef.data));
			_fileRef.removeEventListener(Event.SELECT, onFileLoadSelected);
			_fileRef.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			_fileRef.removeEventListener(Event.COMPLETE, onFileLoadComplete);
			
			ShowDialogStarts();
		}
		
		private function simulateOnFileLoadComplete(dialogXML:XML):void
		{
			ShowInfoMessage("simulace fileloadu");
			_dialogFlow = new DialogFlow(new XML(dialogXML));
			ShowDialogStarts();
		}
		
		private function ShowDialogStarts():void {
			_entryPointsData = new DataProvider(_dialogFlow.GetEntryPointsData());
			_entryPoints.dataProvider = _entryPointsData;
			
			_allReplicsData = new DataProvider(_dialogFlow.GetAllReplicsData());
			_allReplics.dataProvider = _allReplicsData;
		}
	}
}