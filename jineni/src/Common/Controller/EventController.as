package Common.Controller 
{
	import Common.Events.EventManager;
	import Common.Exception.RuntimeInternalError;
	import fl.controls.Button;
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;

	
	public class EventController 
	{		
		protected var _eventManager: EventManager;
		protected var _sprite:Sprite;
		
		public function EventController() 
		{
			_eventManager = new EventManager(this);
		}
		
		public function SetSprite(sprite:Sprite):void {
			_sprite = sprite;
		}
		
		protected function GetSpriteInLoadedGraphic(objectNameInGraphic:String):Sprite {
			if (_sprite[objectNameInGraphic] == null) {
				throw new RuntimeInternalError("object "+objectNameInGraphic+" could not be found in "+_sprite.name);
			}
			return _sprite[objectNameInGraphic] as Sprite;
		}
		
		protected function GetObjectInLoadedGraphic(objectNameInGraphic:String, type:*):* {
			if (_sprite[objectNameInGraphic] == null) {
				throw new RuntimeInternalError("object "+objectNameInGraphic+" could not be found in "+_sprite.name);
			}
			return _sprite[objectNameInGraphic] as type;
		}
	
		
		public function RemoveListeners(): void {
			_eventManager.RemoveListeners();
		}
		
		public function AttachListeners(): void {
			_eventManager.AttachListeners();
		}
	}

}