package Common.Controller 
{
	
	
	import Common.Exception.RuntimeInternalError;
	import Common.World.AllGameObjectsOLD;
	import br.com.stimuli.loading.BulkProgressEvent;
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.loadingtypes.LoadingItem;
	import fl.data.DataProvider;
	import flash.display.Sprite;
	import Common.World.CoreGameObjects;

	import flash.events.ErrorEvent;
	
	public class ControllerWithFilesLoader extends EventController
	{
		protected var _loader:BulkLoader;
		protected var _main:Main;
		protected var _gameObjects:AllGameObjectsOLD;
		protected var _coreGameObjects:CoreGameObjects;
		
		public function ControllerWithFilesLoader(main:Main, loader:BulkLoader) 
		{
			_main = main;
			_loader = loader;
		}
		
		
		protected function onAllLoaded(evt:BulkProgressEvent):void
		{
			trace("onAllLoaded SHOULD BE OVERRIDEN"+evt.target);
		}
		
		protected function onAllProgress(evt:BulkProgressEvent):void
		{
			//!! i dont need progress right now
			//trace("onAllProgress SHOULD BE OVERRIDEN"+evt.target);
		}
		
		protected function onAllError(evt:ErrorEvent):void
		{
			trace("onAllError SHOULD BE OVERRIDEN"+BulkLoader(evt.target).getFailedItems());
		}
		
		protected function ConfigXMLSettings():void {
			XML.ignoreComments = false;
			XML.ignoreProcessingInstructions = false;
		}
		/*
		protected function AddCoreXmlFiles():void {
			_loader.add("xml/core/objects.xml", {id: "objects", type: BulkLoader.TYPE_XML});
			_loader.add("xml/core/items.xml", {id: "items", type: BulkLoader.TYPE_XML});
			_loader.add("xml/core/locations.xml", {id: "locations", type: BulkLoader.TYPE_XML});
		}*/
		
		protected function RunLoader():void {
			_loader.addEventListener(BulkLoader.COMPLETE, onAllLoaded);
			_loader.addEventListener(BulkLoader.PROGRESS, onAllProgress);
			_loader.addEventListener(BulkLoader.ERROR, onAllError);
			
			_loader.start();
		}
		
		protected function CreateCoreGameObjects():void {
			_coreGameObjects = new CoreGameObjects();
		}
		
		public function GetLocationsData():DataProvider {
			return _coreGameObjects.GetLocationsData();
		}
		
		public function GetDialogObjectsData():DataProvider {
			return _coreGameObjects.GetLocationsData();
		}
		
		public function GetItemsData():DataProvider {
			return _coreGameObjects.GetItemsData();
		}
		
		protected function SetEventManagement(sprite: Sprite, eventHandlersForTargets:Array, activateImmediately:Boolean=false):void{
			_eventManager.InterlinkWithData(sprite, eventHandlersForTargets, activateImmediately);
			_eventManager.AttachListeners();
		}
		
	}

}