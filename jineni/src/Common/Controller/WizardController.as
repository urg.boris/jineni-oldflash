package Common.Controller 
{
	//import Common.DataContainer.EventHandlersContainer;
	import Common.Exception.RuntimeInternalError;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class WizardController extends EventController
	{
		protected var _parentController:EventController;
		protected var _parentSprite: Sprite;
		protected var _spriteName:String;
		//protected var _eventHandlersContainer: EventHandlersContainer;
		protected var _eventHandlersData:Array;

		
		public function WizardController(parentController: EventController, sprite:Sprite, activateImmediately:Boolean=false) 
		{
			_parentController = parentController;	
			SetSprite(sprite);
			InterlinkWithData(activateImmediately);
			_sprite.visible = false;
		}
		
		private function InterlinkWithData(activateImmediately:Boolean=false): void {
			_eventManager.InterlinkWithData(_sprite, _eventHandlersData, activateImmediately);
		}
		
		public function OpenWizzard():void {
			_sprite.visible = true;
			AttachListeners();
		}
		
		public function Close():void {
			_eventManager.RemoveListeners();
			_sprite.visible = false;
		}
		
		public function Hide():void {
			_sprite.visible = false;
		}
	}

}