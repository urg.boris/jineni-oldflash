package Common.World 
{
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	import Common.Exception.RuntimeInternalError;
	import Common.World.GameObject.DialogObject;
	import Common.World.GameObject.Item;
	import Common.World.GameObject.Location;
	import Common.World.List.DialogObjects;
	import Common.World.List.Items;
	import Common.World.List.Locations;
	import DialogEditor.DialogObject.Replic;
	import Common.Utils.StringConst;
	import fl.data.DataProvider;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class CoreGameObjects
	{
		public var _items:Items;
		public var _locations:Locations;
		public var _dialogObjects:DialogObjects;
		
		public function CoreGameObjects() 
		{
			_items = new Items();
			_locations = new Locations();
			_dialogObjects = new DialogObjects();
		}
		
		public function TraceContent():void {
			_items.traceList();
		}
		
		public function GetItemsData():DataProvider {
			return _items.GetAsDataProvider();
		}
		
		public function GetItem(id:String):Item {
			return _items.GetById(id);
		}
		
		public function GetLocationsData():DataProvider {
			return _locations.GetAsDataProvider();
		}
		
		public function GetLocation(id:String):Location {
			return _locations.GetById(id);
		}
		
		public function GetDialogObjectsData():DataProvider {
			return _dialogObjects.GetAsDataProvider();
		}
		
		public function GetDialogObject(id:String):DialogObject {
			return _dialogObjects.GetById(id);
		}
	}
}