package Common.World 
{
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	import Common.Exception.RuntimeInternalError;
	import DialogEditor.DialogObject.Replic;
	import Common.Utils.StringConst;
	import fl.data.DataProvider;
	
	/**
	 * ...
	 * @author Plesnif
	 */
	public class AllGameObjectsOLD 
	{
		private var _locations:XML;
		private var _dialogObjects:XML;
		private var _items:XML;
		
		public var _dialogObjectsData:DataProvider;
		public var _locationsData:DataProvider;
		public var _itemsData:DataProvider;
		
		//TODO neudelat nestatic
		public function AllGameObjectsOLD(locations:XML, dialogObjects: XML, items:XML) 
		{
			if (locations.toXMLString() === "")
			{
				throw new RuntimeInternalError("loaded locations is empty");
			}
			if (dialogObjects.toXMLString() === "")
			{
				throw new RuntimeInternalError("loaded characters is empty");
			}
			if (items.toXMLString() === "")
			{
				throw new RuntimeInternalError("loaded items is empty");
			}

			_items = items;
			_dialogObjects = dialogObjects;
			_locations = locations;		
			
			_itemsData = TransformXmlToData(items);
			_dialogObjectsData = TransformXmlToData(dialogObjects);
			_locationsData = TransformXmlToData(locations);
		}
		
		private function getGameObjectNameFromXML(xml:XML, childName:String, objectId:String, nameId:String = null): String {
			//!! je zaruceno, vyber 
			var name:String;
			if (nameId == null) {
				name = xml.elements(childName).(@id == objectId).name[0];
			} else {
				name = xml.elements(childName).(@id == objectId).name.(@id == nameId);
			}
			if (name == "" || name==null) {
				throw new WronglyFormattedDialogXML("cant reach object name: object="+objectId+" nameId="+(!nameId ? "default" : nameId));
			}
			return name;
		}
		public function GetLocationName(objectId:String, nameId:String = null): String {
			return getGameObjectNameFromXML(_locations, StringConst.LOCATION, objectId, nameId);
		}
		
		public function GetDialogObjectName(objectId:String, nameId:String = null): String {
			return getGameObjectNameFromXML(_dialogObjects, StringConst.OBJECT, objectId, nameId);
		}
		
		public function GetItemName(objectId:String, nameId:String = null): String {
			return getGameObjectNameFromXML(_items, StringConst.ITEM, objectId, nameId);
		}
		
		public function GetLocationsData():DataProvider {
			return _locationsData;
		}
		
		public function GetDialogObjectsData():DataProvider {
			return _dialogObjectsData;
		}
		
		public function GetItemsData():DataProvider {
			return _itemsData;
		}
		
		protected function TransformXmlToData(xmlData:XML):DataProvider
		{
			var itemLabel:String = "";
			var itemData:String;
			var list:DataProvider = new DataProvider();
			var subLength:int;
			for (var i:int = 0; i < xmlData.children().length(); i++)
			{
				subLength = xmlData.children()[i].elements(StringConst.NAME).length();
				for (var j:int = 0; j < subLength; j++)
				{
					itemLabel += xmlData.children()[i].elements(StringConst.NAME)[j];
					if (j != subLength - 1)
					{
						itemLabel += ", ";
					}
				}
				itemData = new String(xmlData.children()[i].@id);
				itemLabel = itemData+" - "+itemLabel;
				list.addItem({label: itemLabel, data: itemData});
				itemLabel = "";
			}
			return list;
		}
	}

}