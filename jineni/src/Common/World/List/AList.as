package Common.World.List 
{
	import Common.Exception.RuntimeInternalError;
	import Common.World.GameObject.AGameObject;
	import Common.World.GameObject.Item;
	import fl.data.DataProvider;
	/**
	 * ...
	 * @author Plesnif
	 */
	public class AList 
	{
		protected var _list:Array;
		protected var _hashMap:Object;
		protected var _dataProvider:DataProvider;
		protected var _listElement:Class;
		
		public function AList() 
		{
			_list = new Array();
			_hashMap = new Object();
		}
		
		protected function AddElement(id:String, name:Array):void {
			var newIndex: int = _list.push(new _listElement(id, name));
			 _hashMap[id] = newIndex - 1;
			 trace("AddElement" + (newIndex - 1));
		}
		
		public function GetById(id:String):* {
			if (_hashMap[id] === undefined) {
				trace("eee");
				throw new RuntimeInternalError("element "+id+ " cannot be found in list of "+_listElement);
			}
			return _list[_hashMap[id]];
		}
		
		public function traceList():void {
			for (var i:int = 0; i < _list.length-1; i++) {
				trace((_list[i] as AGameObject).GetWholeName());
			}
		}
		
		public function GetAsDataProvider():DataProvider {
			if (_dataProvider == null) {
				CreateDataProvider();
			}
			return _dataProvider;
		}
		
		public function CreateDataProvider():void {
			_dataProvider = new DataProvider();
			
			var item:AGameObject;
			for (var i:int = 0; i < _list.length; i++) {
				item = _list[i] as AGameObject;
				_dataProvider.addItem({label: item.GetWholeName(), data: item.GetId()});
			}
		}
	}

}