package Common.World.List 
{
	import Common.World.GameObject.Item;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Plesnif
	 */
	public class Items extends AList 
	{
		
		public function Items() 
		{
			super();
			_listElement = Item;
			//TODO jmena budu formou var map:Object = new Object(); map.name1 = "Lee";
			//AddElement("I1", new Array("N1", "nezmany predmet", "N2", "Figurka slona"));
			AddElement("I1", new Array("Figurka slona"));
			AddElement("I2", new Array("Padesátikoruna"));
			AddElement("I3", new Array("Nahnilý ananas"));
			AddElement("I4", new Array("Punky-Strike"));
			AddElement("I5", new Array("Bota"));
			AddElement("I6", new Array("Mapa"));
			AddElement("I7", new Array("Kladivo"));
			AddElement("I8", new Array("Žebřík"));
			AddElement("I9", new Array("Brýle "));
			AddElement("I10", new Array("Dopis dr.Chorozovicovi"));
			AddElement("I11", new Array("Poznamky S.Pilzenlagera"));
			AddElement("I12", new Array("Kompromitujici materialy"));
			AddElement("I13", new Array("Diamant"));
			AddElement("I14", new Array("Obálka s adresou"));
			AddElement("I15", new Array("Dopis pro tebe"));
			AddElement("I16", new Array("Foťák"));
			AddElement("I17", new Array("Stolek s kazeťákem"));
			AddElement("I18", new Array("Ruka"));
			AddElement("I19", new Array("CD Jinovatka"));
			AddElement("I20", new Array("Houbičky"));
			AddElement("I21", new Array("Baterky"));
			AddElement("I22", new Array("Izolepa"));
			AddElement("I23", new Array("Prasklá žárovka"));
			AddElement("I24", new Array("Deštník"));
			AddElement("I25", new Array("Krabice"));
			AddElement("I26", new Array("Brýle oblepené izolepou"));
			AddElement("I27", new Array("Brýle oblepené izolepou s kousky ananasu"));
			AddElement("I28", new Array("Ranní rozbřesk"));
			AddElement("I29", new Array("Zlý sen"));
			AddElement("I30", new Array("Stolní lustr na deštníky"));
			AddElement("I31", new Array("Koridor B"));
			AddElement("I32", new Array("Koridor B v krabici"));
			AddElement("I33", new Array("Král slonů"));
		}
	}

}