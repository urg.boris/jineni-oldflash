package Common.World.GameObject 
{
	/**
	 * ...
	 * @author Plesnif
	 */
	public class AGameObject 
	{
		public var _id:String;
		public var _name:Array;
		public var _currentNameIndex:int;

		public function AGameObject(id:String, name:Array) 
		{
			_id = id;
			_name = name;
			_currentNameIndex = 0;
		}
		
		public function GetId():String {
			return _id;
		}
		
		public function GetWholeName():String {
			return _id+": "+_name.join(", ");
		}
		
		public function GetCurentName():String {
			return _name[_currentNameIndex];
		}
		
		public function SetCurrentNameIndex():void {
			//todo check if index exists
		}
	}

}