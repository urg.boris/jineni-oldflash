package Common.World.GameObject 
{
	/**
	 * ...
	 * @author Plesnif
	 */
	public class Location extends AGameObject
	{
		private var _exits:Array;
		private var _dialogObjects:Array;
		private var _isOpened:Boolean;
		private var _isOnMap:Boolean;
		public function Location(id:String, name:Array, exits:Array = null, dialogObjects:Array = null) 
		{
			super(id, name);
			if (exits == null) {
				_exits = new Array();
			} else {
				_exits = exits;
			}
			if (dialogObjects == null) {
				_dialogObjects = new Array();
			} else {
				_dialogObjects = dialogObjects;
			}
			_isOpened = false;
			_isOnMap = false;
		}		
		
		public function CheckIfCorespondsWithGrafic():void {
			//TODO checks exits
			//TOSO checks dialog objects
		}
	}

}