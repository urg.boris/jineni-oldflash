package Common.Dialog.Exception 
{
	public class WronglyFormattedDialogXML extends Error 
	{ 
		public function WronglyFormattedDialogXML(message:String) 
		{ 
			super(message); 
		} 
	}

}