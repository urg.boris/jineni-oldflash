package Common.Dialog.DialogObject 
{
	import Common.Exception.RuntimeInternalError;
	import Common.Utils.GeneralTools;
	import Common.Utils.StringConst;
	/**
	 * ...
	 * @author Plesnif
	 */
	public class ReplicPointer 
	{
		public static const SIMPLE_POINTER:int = 1;
		public static const ANSWER_POINTER:int = 2;
		public static const RANDOM_POINTER:int = 3;
		public static const DIALOG_END_POINTER:int = 4;
		
		private var _pointsTo:Array;
		private var _type:int;
		
		public function ReplicPointer(type:int, pointsTo:Array) 
		{
			_pointsTo = pointsTo;
			_type = type;
		}
		
		public function IsSimplePointer():Boolean {
			return _type == SIMPLE_POINTER;
		}
		
		public function IsOptionPointer():Boolean {
			return _type == ANSWER_POINTER;
		}
		
		public function IsRandomPointer():Boolean {
			return _type == RANDOM_POINTER;
		}
		
		public function IsDialogEnd():Boolean {
			return _type == DIALOG_END_POINTER;
		}
		
		public function GetNextReplicId():String {
			if (IsSimplePointer()) {
				return _pointsTo[0];
			}
			
			if (IsDialogEnd()) {
				return StringConst.DIALOG_END;
			}
			
			if (IsRandomPointer()) {
				var i:int = GeneralTools.GetRandomInt(_pointsTo.length);
				return _pointsTo[i];
			}
		
			if (IsOptionPointer()) {
				throw new RuntimeInternalError("GetNextReplic: canot get next replic for OPTION_POINTER type");
			}
			throw new RuntimeInternalError("GetNextReplic: shouldnt be reaching this point");
		}
		
	}

}