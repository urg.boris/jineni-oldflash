package Common.Dialog.DialogObject
{
	import DialogEditor.DialogEditorController;
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	import flash.display.Sprite;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	//import Common.Controller.BaseController;
	import DialogEditor.Utils.StringConst;
	import Common.Utils.XmlTools;
	
	public class Replic
	{
		private var _id:String;
		private var _text:String;
		private var _isEntryPoint: Boolean = false;
		private var _dialogObjectId:String;
		private var _next:ReplicPointer;		
		
		public function Replic(replicXML:XML)
		{			
			if (!XmlTools.hasAttributeWithName(replicXML, StringConst.ID)) {
				throw new WronglyFormattedDialogXML("attribute @" + StringConst.ID + " nanalezen v replice " + replicXML.toXMLString());
			}			
			_id = replicXML.@[StringConst.ID];	
			
			if (!XmlTools.hasAttributeWithName(replicXML, StringConst.DIALOG_OBJECT)) {
				throw new WronglyFormattedDialogXML("attribute @" + StringConst.DIALOG_OBJECT + " nanalezen v replice id=" + _id);
			}			
			_dialogObjectId = replicXML.@[StringConst.DIALOG_OBJECT];
			
			if (!XmlTools.hasOnlyChildWithName(replicXML, StringConst.TEXT)) {
				throw new WronglyFormattedDialogXML("jine mnozstvi " + StringConst.TEXT + " potomku nez 1 v replice id=" + _id);
			}
			_text = replicXML.elements(StringConst.TEXT);
			
			if (XmlTools.attributeEqualsTo(replicXML, StringConst.IS_ENTRY_POINT, "1")) {
				this._isEntryPoint = true;
			}
			
			if (!XmlTools.hasOnlyChildWithName(replicXML, StringConst.POINTS_TO)) {
				throw new WronglyFormattedDialogXML("jine mnozstvi " + StringConst.POINTS_TO + " potomku nez 1 v replice id=" + _id);
			}
			
			var pointsTo:XMLList = replicXML.elements(StringConst.POINTS_TO).children();			
			var pointsToOptions:int = pointsTo.length();
			var isDialogEnd:Boolean = pointsTo[0].name() == StringConst.DIALOG_END;
			var isRandomPointer:Boolean = XmlTools.attributeEqualsTo(replicXML, StringConst.IS_RANDOM_POINTER, "1");
			
			if ((pointsToOptions > 1 && isDialogEnd) || (isDialogEnd && isRandomPointer) || (pointsToOptions == 1 && isRandomPointer)) {
				throw new WronglyFormattedDialogXML("consistenci error");
			}
			
			if(isDialogEnd) {
				_next = new ReplicPointer(ReplicPointer.DIALOG_END_POINTER, new Array());
			} else {
				if (pointsToOptions == 1) {
					_next = new ReplicPointer(ReplicPointer.SIMPLE_POINTER, new Array(pointsTo[0].@[StringConst.ID]));
				} else {
					var ids:Array = new Array();
					for each(var pointer:XML in pointsTo) {
						ids.push(pointer.@[StringConst.ID]);
					}
					if (isRandomPointer) {
						_next = new ReplicPointer(ReplicPointer.RANDOM_POINTER, ids);
					} else {
						//!!more ids, is option pointer type
						_next = new ReplicPointer(ReplicPointer.ANSWER_POINTER, ids);
					}
				}
			}			
			
			
			/*
			var triggersCount:int = XmlTools.countChildrenWithName(replicXML, XmlTypeNames.TRIGGER);
			if (triggersCount > 1) {
				throw new WronglyFormattedDialogXML("vetsi mnozstvi " + XmlTypeNames.TRIGGER + " potomku nez 1 v replice id=" + _id);
			}
			if (triggersCount == 1) {
				_triggers = replicXML.elements(XmlTypeNames.TRIGGER);
			}*/
			

		}
		
		public function GetId():String 
		{
			return _id;
		}
		
		public function DialogContinues():Boolean {
			return !_next.IsDialogEnd();
		}
		
		public function IsDialogEnd():Boolean {
			return _next.IsDialogEnd();
		}
		
		public function IsRandomPointer():Boolean {
			return _next.IsRandomPointer();
		}
		
		public function IsOptionPointer():Boolean {
			return _next.IsOptionPointer();
		}
		
		public function GetNext():String
		{
			if (!IsOptionPointer()) {
				return _next.GetNextReplicId();
			} else {
				return "isoptionpointer";
			}
			
		}
		

		/*
		public function getNext():XMLList 
		{
			return this._next;
		}*/
		/*
		public function GetNextId():String {
			return _next.children()[0].@[XmlTypeNames.ID];
		}
		
		public function getSuccessorsCount():int
		{
			return this._successorsCount;
		}		
		
		public function addSuccessor(replic:Replic):void {
			this._successors.push(replic);
			this._successorsCount += replic.getSuccessorsCount() + 1;
		}
		
		public function getSuccessors(): Array {
			return this._successors;
		}
		
		public function isEntryPoint():Boolean {
			return this._isEntryPoint;
		}*/
	}
}