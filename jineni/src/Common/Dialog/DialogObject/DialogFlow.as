package Common.Dialog.DialogObject
{
	import Common.Dialog.Exception.WronglyFormattedDialogXML;
	import Common.Exception.RuntimeInternalError;
	import Common.World.AllGameObjectsOLD;
	import DialogEditor.Utils.StringConst;
	import flash.utils.Dictionary;
	
	public class DialogFlow 
	{
		private var _replicsData:Array;
		private var _entryPointsData:Array;
		private var _replics:Dictionary;
		//private var _dialogXML:XML;
		private var _isRuning:Boolean;
		private var _actualReplic:Replic;
		
		public function DialogFlow(dialogXML:XML)
		{
			if (dialogXML.toXMLString() === "")
			{
				throw new WronglyFormattedDialogXML("DialogFlow: loaded dialog is empty");
			}
			_isRuning = false;
			CreateReplicsFromXml(dialogXML);
		}
		
		public function RunDialog(entryPoint:String = null):void {
			if (_isRuning == true) {
				throw new RuntimeInternalError("DialogFlow already running");
			}
			_isRuning = true;
		}
		
		public function CreateReplicsFromXml(dialogXML:XML):void
		{			
			_replics = new Dictionary();
			_entryPointsData = new Array();
			_replicsData = new Array();			
			var i:Boolean = false;
			for each(var replic:XML in dialogXML.replic) {

				var replicObject:Replic = new Replic(replic);
				if (i == false) {
					i = true;
					_actualReplic = replicObject;
				}
				_replics[replicObject.GetId()] = replicObject;
				_replicsData.push({label:replic.text, data:replic.@[StringConst.ID]});
				if (replic.hasOwnProperty('@' + StringConst.IS_ENTRY_POINT) && replic.@[StringConst.IS_ENTRY_POINT] == '1') {
					_entryPointsData.push({label:replic.text, data:replic.@[StringConst.ID]});
				}

				trace(replicObject.GetId() + "   " + replicObject.GetNext());
				
			}
			
			if (_entryPointsData.length == 0) {
				throw new WronglyFormattedDialogXML("no entry point for dialog");
			}
			MoveToNextReplic();
		}
		
		public function GetEntryPointsData():Array {
			return _entryPointsData;
		}
		
		public function GetAllReplicsData():Array {
			return _replicsData;
		}
		
		private function MoveToNextReplic():void {
			//FindReplicById(_actualReplic.GetNextId());
		}
		
		private function FindReplicById(id:String):void {
			//trace((_replics[id] as Replic).getId());
		}
		
		
		
		
		/*
		public function isEntryPoint(id:String):Boolean {
			var replicXML:XMLList = this._dialogXML.replic.(@[XmlTypeNames.ID] == id);
			if (replicXML.(hasOwnProperty('@' + XmlTypeNames.IS_ENTRY_POINT) && @[XmlTypeNames.IS_ENTRY_POINT] == '1')) {
				return true;
			} else {
				return false;
			}
		}*/
/*
		public function addReplic(replica:Replic):void
		{
			this._replics.push(replica);
		}
		*/
		/*
		public function addEntryPoint(replica:Replic):void
		{
			this._entryPoints.push(replica);
		}
		
		public function getEntryPoints():Array
		{
			return this._entryPoints;
		}
		*/
		/*
		public function getAllReplics(): Array {
			var allReplics:XMLList = this._dialogXML.replic;
			var replicIds:Array = new Array();
			for each(var replic:XML in allReplics) {
				replicIds.push(replic.@id.toString());
			}
			return replicIds;
		}
		*/
		
		/*
		public function replicExists(id:String): Boolean {
			var replicXML:XMLList = this._dialogXML.replic.(@[XmlTypeNames.ID] == id);
			if (replicXML.toXMLString()!="") {
				return true;
			} else {
				return false;
			}
		}
		
		public function getReplicAsXMLListById(id:String):XMLList {
			var replicXML:XMLList = this._dialogXML.replic.(@[XmlTypeNames.ID] == id);
			if (this._dialogXML.replic.(@[XmlTypeNames.ID] == id).length() > 1) {
				throw new WronglyFormattedDialogXML("replic id=" + id + " se vyskytuje vicekrat");
			}
			if (replicXML.toXMLString() == "")
			{
				throw new WronglyFormattedDialogXML("replic id=" + id + " neexistuje");
			}
			else
			{	
				return replicXML;
			}
		}
		
		public function getReplicAsXMLById(id:String):XML {
			var replicsXML:XMLList = this._dialogXML.replic.(@[XmlTypeNames.ID] == id);
			if (replicsXML.length() > 1) {
				throw new WronglyFormattedDialogXML("replic id=" + id + " se vyskytuje vicekrat");
			}
			if (replicsXML.toXMLString() == "")
			{
				throw new WronglyFormattedDialogXML("replic id=" + id + " neexistuje");
			}
			else
			{	
				return replicsXML[0];
			}
		}
		
		public function GetReplicTextById(id:String):String {
			return getReplicAsXMLById(id).elements("text").toString();
		}
		
		public function createReplicFromXMLById(id:String):Replic {
			return new Replic(getReplicAsXMLListById(id), this);
		}
		
		public function getContent(): XML {
			return _dialogXML;
		}
		
		public function GetPointsToIdsFrom(id:String):Array {
			var ids:Array = new Array();
			var replic:XML = getReplicAsXMLById(id);
			for each(var replicNode:XML in replic.pointsTo.replicNode){
				trace(replicNode.@id);
				ids.push({data:replicNode.@id, label:replicNode.@id + ": "+GetReplicTextById(replicNode.@id)})
			}
			return ids;
		}
		*/
	}
}