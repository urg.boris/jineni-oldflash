package Common.Events 
{
	import Common.Controller.EventController;
	import Common.Exception.RuntimeInternalError;
	import fl.controls.Button;
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import fl.controls.List;

	//TODO rozsirit na to ze budou nejen cklick eventy
	public class EventManager 
	{
		protected var _targetTypes:Array = [SimpleButton, Button, List];
		protected var _sprites:Array = new Array();
		//!! must be function/and functionName because of eventlisteners
		protected var _events:Array =  new Array();
		protected var _eventNames:Array =  new Array();
		protected var _eventTypes: Array = new Array();
		protected var _attachOn:Sprite;
		protected var _controller:EventController
		
		public function EventManager(controller:EventController) 
		{
			_controller = controller;
		}
		
		public function InterlinkWithData(sprite: Sprite, eventHandlersForTargets:Array, activateImmediately:Boolean=false):void {
			_attachOn = sprite;
			for each (var eventHandlerForTarget:Object in eventHandlersForTargets) 
			{
				PrepareEventOn(eventHandlerForTarget, activateImmediately);
			}
		}
		
		protected function PrepareEventOn(eventData:Object, activateImmediately:Boolean=false):void {			
			var targetGraphic:DisplayObject = _attachOn[eventData["spriteName"]];
			
			if (targetGraphic == null) {
				throw new RuntimeInternalError("could not map event on graphic, graphic " + eventData["spriteName"] + " not found");
			}
			
			//!!type must exists
			if (_targetTypes.indexOf(eventData["type"]) == -1) 
			{
				throw new RuntimeInternalError(eventData["spriteName"] + " with type "+eventData["type"]+" could not attach event ");
			}
			var target:* = targetGraphic as eventData["type"];

			//!! function eventHandlerForSwfTarget["handlerName"] musi byt "public"
			if (!eventData["eventHandlerName"] in _controller) {	
				throw new RuntimeInternalError(eventData["spriteName"] + " with event "+eventData["eventHandlerName"]+" could not be attach, "+eventData["eventHandlerName"]+" is not function");
			}
			var handler: Function = _controller[eventData["eventHandlerName"]] as Function;
			
			var eventType:String;
			//!! default value is mouse click
			if (eventData["eventType"] == undefined) {
				eventType = MouseEvent.CLICK;
			} else {
				eventType = eventData["eventType"];
			}
			
			AddToList(target, handler, eventData["eventHandlerName"], eventType);
			if (activateImmediately) {
				target.addEventListener(eventType, handler);
			}
		}
		
		public function isEmpty():Boolean {
			return _sprites.length == 0;			
		}
		
		
		
		private function IntegrityCheck(): void {
			if (!(_sprites.length == _events.length && _events.length == _eventNames.length && _eventNames.length == _eventTypes.length)) {
				throw new RuntimeInternalError("integrity error: _sprites.length != _events.length != _eventNames.length != _eventTypes.length - " + _sprites.length + "!=" + _events.length + "!=" + _eventNames.length+"!=" + _eventTypes.length);
			}
		}
		
		
		public function AddToList(sprite:Sprite, eventHandler:Function, eventName:String, eventType:String, checkIntegrity:Boolean = true): void {
			if (checkIntegrity == true) {
				IntegrityCheck();
			}			
			_sprites.push(sprite);
			_events.push(eventHandler);
			_eventNames.push(eventName);
			_eventTypes.push(eventType);
		}
		
		public function RemoveListeners(): void {
			IntegrityCheck();
			var sprite:Sprite;
			var event:Function;
			var eventName:String;
			var eventType:String;
			for (var i:int = 0; i < _sprites.length; i++) {
				sprite = (_sprites[i] as Sprite);
				eventName = (_eventNames[i] as String);
				event = (_events[i] as Function);
				eventType = (_eventTypes[i] as String);
				
				if (!sprite.hasEventListener(eventType)) {
					throw new RuntimeInternalError("integrity error: cant remove eventlistener " + eventName + " from sprite " + sprite.name+" because it is not there");
				}
				sprite.removeEventListener(eventType, event);
			}
		}
		
		public function AttachListeners(): void {
			IntegrityCheck();
			var sprite:Sprite;
			var event:Function;
			var eventName:String;
			var eventType:String;
			for (var i:int = 0; i < _sprites.length; i++) {
				sprite = (_sprites[i] as Sprite);
				eventName = (_eventNames[i] as String);
				event = (_events[i] as Function);
				eventType = (_eventTypes[i] as String);
				
				if (sprite.hasEventListener(eventType)) {
					throw new RuntimeInternalError("integrity error: cant add eventlistener " + eventName + " to sprite " + sprite.name+" because it is already there");
				}
				sprite.addEventListener(eventType, event);
			}
		}
		
		
	}

}