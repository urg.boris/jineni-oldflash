package Common.Utils 
{
	import com.sociodox.utils.Base64;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	import flash.filesystem.FileMode;
	/**
	 * ...
	 * @author Plesnif
	 */
	public class GeneralTools 
	{
		public static function GetRandomInt(max:int, min:int = 0):int {
			if (min == 0) {
				return Math.floor(Math.random() * max);
			}
			return (Math.floor(Math.random() * (max - min + 1)) + min);
		}
		
		public static function EncodeString(value: String): String
		{
			/*
			const source: ByteArray = new ByteArray();
			source.writeUTFBytes(value);
			return encode(source);*/
			return "";
		}

		public static function DecodeToString(str: String): String
		{
			//var decoder:Base64 = new Base64();
			return Base64.decode(str).toString();
		}
		
		public static function SerializeObjectToFile(object:Object, filePath:String):void {
			var bytes:ByteArray = new ByteArray();
			bytes.writeObject(object);			
			var fs:FileStream = new FileStream();
			var file:File = new File(File.applicationDirectory.resolvePath(filePath).nativePath);
			fs.open(file, FileMode.WRITE);
			fs.writeBytes(bytes,0,bytes.bytesAvailable);
			fs.close();
		}
		
		public static function DeserializeObjectFromFile(filePath:String):Object {
		   var bytes:ByteArray = new ByteArray();
		   var fs:FileStream = new FileStream();
		   var file:File = new File(File.applicationDirectory.resolvePath(filePath).nativePath);
		   fs.open(file, FileMode.READ);
		   fs.readBytes(bytes, 0, fs.bytesAvailable);
		   //fs.close();
		   return bytes.readObject();
		}
		
	}

}