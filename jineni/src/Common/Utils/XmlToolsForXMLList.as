package Common.Utils 
{
	/**
	 * ...
	 * @author Plesnif
	 */
	public class XmlToolsForXMLList 
	{		
		public static function hasElement(xml: XMLList, elementName:String): Boolean {
			if (xml.elements(elementName).length()>0) {
				return true;
			} else {
				return false;
			}
		}
		
		public static function hasAttributeWithName(xml: XMLList, name:String):Boolean {
			return xml.attribute(name).length() > 0;//(replicXML.(hasOwnProperty('@' + name)) && replicXML.@[name].toXMLString() != "");
		}
		
		public static function attributeEqualsTo(xml: XMLList, attributeName:String, attributeValue:String):Boolean {
			if (hasAttributeWithName(xml, attributeName)) {
				if (xml.@[attributeName].toXMLString() == attributeValue) {
					return true;
				}
			}
			return false;
		}
		
		public static function hasOnlyChildWithName(xml: XMLList, name:String):Boolean {
			return countChildrenWithName(xml, name) == 1;
		}
		
		public static function countChildrenWithName(xml: XMLList, name:String):int {
			return xml.elements(name).length();
		}		
	}

}