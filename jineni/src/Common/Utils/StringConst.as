package Common.Utils 
{
	/**
	 * ...
	 * @author Plesnif
	 */
	public class StringConst 
	{
		public static const DIALOG_END:String = "dialogEnd";
		public static const LOCATION:String = "location";
		public static const OBJECT:String = "object";
		public static const ITEM:String = "item";
		public static const NAME:String = "name";
	}

}