package Common.Utils 
{
	/**
	 * ...
	 * @author Plesnif
	 */
	public class Checker 
	{
		//!!(location_person|object_X) | (item_X) + examine
		public static var validReplicId:RegExp = /L[0-9]+_[PO][0-9]+(_E)?(_[0-9]+[A-Z]?)+|I[0-9]+(_E)?(_[0-9]+[A-Z]?)+/;
		public static function isValidReplicId(id:String): Boolean {
			if (id.match(validReplicId)) {
				return true;
			} else {
				return false;
			}
		}
		
		public static var validCharacterId:RegExp = /P[0-9]+/;
		public static function isValidCharacterId(id:String): Boolean {
			if (id.match(validCharacterId)) {
				return true;
			} else {
				return false;
			}
		}
	}

}