package Common.Exception 
{
	public class RuntimeInternalError extends Error 
	{ 
		public function RuntimeInternalError(message:String) 
		{ 
			super(message); 
		} 
	}

}