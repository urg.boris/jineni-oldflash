package 
{
	
	import DialogEditor.DialogEditorController;
	import DialogTester.DialogTesterController;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import com.sociodox.theminer.TheMiner;

	public class Main extends Sprite 
	{
		private var _container:Sprite = new Sprite();
		private var _loader:Loader = new Loader(); 
		public var _stage:Stage;
/*		private static const STAGE_WIDTH:int = 1000;
        private static const STAGE_HEIGHT:int = 800;*/
		
		public function Main() 
		{		
			addEventListener(Event.ADDED_TO_STAGE, CreateUniverse);
			

		}
		
		private function CreateUniverse(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, CreateUniverse); 
			if (CONFIG::debug) {
				this.addChild(new TheMiner()); 
			}
			 
			stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
			_stage = stage;
			var path:String = "graphic/mainCrossroad.swf" 
			var pathURLRequest:URLRequest = new URLRequest(path); 
			_loader.load(pathURLRequest); 
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadCompleted);
		}
		
		private function loadCompleted(event:Event):void 
		{
			/*
			
			var controller:DialogTesterController = new DialogTesterController(this);
			controller.run();
			*/
			
			var controller:DialogEditorController = new DialogEditorController(this);
			controller.run();
			
			/*
			_loader.content["RunGame"].addEventListener(MouseEvent.CLICK, runGame);
			_loader.content["RunDialogEditor"].addEventListener(MouseEvent.CLICK, runDialogEditor);
			_loader.content["RunDialogTester"].addEventListener(MouseEvent.CLICK, runDialogTester);
			_loader.content["RunGameTester"].addEventListener(MouseEvent.CLICK, runGameTester);
			_container = _loader.content as Sprite;
			addChild(_container);  */
			
		}
		
		private function RemoveListeners():void {
			_loader.content["RunGame"].removeEventListener(MouseEvent.CLICK, runGame);
			_loader.content["RunDialogEditor"].removeEventListener(MouseEvent.CLICK, runDialogEditor);
			_loader.content["RunDialogTester"].removeEventListener(MouseEvent.CLICK, runDialogTester);
			_loader.content["RunGameTester"].removeEventListener(MouseEvent.CLICK, runGameTester);
		}
		
		private function runGame(event:MouseEvent):void {
			trace("RunGame");
			RemoveListeners();
		}
		
		private function runDialogEditor(event:MouseEvent):void {
			trace("runDialogEditor");
			RemoveListeners()
			removeChild(_container);
			var controller:DialogEditorController = new DialogEditorController(this);
			controller.run();
		}
		
		private function runDialogTester(event:MouseEvent):void {
			trace("runDialogTester");
			RemoveListeners()
			removeChild(_container);
			var controller:DialogTesterController = new DialogTesterController(this);
			controller.run();
		}
		
		private function runGameTester(event:MouseEvent):void {
			trace("runGameTester");
			RemoveListeners()
		}
		
	}
	
}